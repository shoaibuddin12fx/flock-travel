(self["webpackChunkVeenme"] = self["webpackChunkVeenme"] || []).push([["src_app_pages_trips_trips_module_ts"],{

/***/ 8031:
/*!****************************************************!*\
  !*** ./src/app/pages/trips/trip/trip.component.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TripComponent": () => (/* binding */ TripComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_trip_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./trip.component.html */ 1321);
/* harmony import */ var _trip_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./trip.component.scss */ 9902);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);




let TripComponent = class TripComponent {
    constructor() { }
    ngOnInit() { }
};
TripComponent.ctorParameters = () => [];
TripComponent.propDecorators = {
    item: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }]
};
TripComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'trip',
        template: _raw_loader_trip_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_trip_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], TripComponent);



/***/ }),

/***/ 1718:
/*!*************************************************!*\
  !*** ./src/app/pages/trips/trip/trip.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TripComponentModule": () => (/* binding */ TripComponentModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _trip_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./trip.component */ 8031);





let TripComponentModule = class TripComponentModule {
};
TripComponentModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [_trip_component__WEBPACK_IMPORTED_MODULE_0__.TripComponent],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.IonicModule],
        exports: [_trip_component__WEBPACK_IMPORTED_MODULE_0__.TripComponent],
    })
], TripComponentModule);



/***/ }),

/***/ 765:
/*!*****************************************************!*\
  !*** ./src/app/pages/trips/trips-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TripsPageRoutingModule": () => (/* binding */ TripsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _trips_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./trips.page */ 7295);




const routes = [
    {
        path: '',
        component: _trips_page__WEBPACK_IMPORTED_MODULE_0__.TripsPage
    }
];
let TripsPageRoutingModule = class TripsPageRoutingModule {
};
TripsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], TripsPageRoutingModule);



/***/ }),

/***/ 4629:
/*!*********************************************!*\
  !*** ./src/app/pages/trips/trips.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TripsPageModule": () => (/* binding */ TripsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _trips_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./trips-routing.module */ 765);
/* harmony import */ var _trips_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./trips.page */ 7295);
/* harmony import */ var src_app_components_header_header_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/header/header.module */ 4546);
/* harmony import */ var _trip_trip_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./trip/trip.module */ 1718);









let TripsPageModule = class TripsPageModule {
};
TripsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.IonicModule,
            _trips_routing_module__WEBPACK_IMPORTED_MODULE_0__.TripsPageRoutingModule,
            src_app_components_header_header_module__WEBPACK_IMPORTED_MODULE_2__.HeaderModule,
            _trip_trip_module__WEBPACK_IMPORTED_MODULE_3__.TripComponentModule,
        ],
        declarations: [_trips_page__WEBPACK_IMPORTED_MODULE_1__.TripsPage],
    })
], TripsPageModule);



/***/ }),

/***/ 7295:
/*!*******************************************!*\
  !*** ./src/app/pages/trips/trips.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TripsPage": () => (/* binding */ TripsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_trips_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./trips.page.html */ 3673);
/* harmony import */ var _trips_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./trips.page.scss */ 6382);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 4282);





let TripsPage = class TripsPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.trips = [
            1,
            2,
            3
        ];
    }
    ngOnInit() {
        // this.trips = this.dataService.getTrips();
    }
};
TripsPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
TripsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-trips',
        template: _raw_loader_trips_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_trips_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], TripsPage);



/***/ }),

/***/ 9902:
/*!******************************************************!*\
  !*** ./src/app/pages/trips/trip/trip.component.scss ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-item {\n  --border-radius: 15px;\n}\n\n.title {\n  color: var(--ion-color-tertiary);\n  font-weight: bold;\n  margin-bottom: 10px;\n}\n\n.duration {\n  font-size: 10px;\n  color: red;\n  margin-top: 3px;\n}\n\n.description {\n  margin-top: 10px;\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRyaXAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxxQkFBQTtBQUNGOztBQUVBO0VBQ0UsZ0NBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBQ0Y7O0FBRUE7RUFDRSxlQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUFDRjs7QUFFQTtFQUNFLGdCQUFBO0VBQ0EsZUFBQTtBQUNGIiwiZmlsZSI6InRyaXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taXRlbSB7XHJcbiAgLS1ib3JkZXItcmFkaXVzOiAxNXB4O1xyXG59XHJcblxyXG4udGl0bGUge1xyXG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnkpO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5kdXJhdGlvbiB7XHJcbiAgZm9udC1zaXplOiAxMHB4O1xyXG4gIGNvbG9yOiByZWQ7XHJcbiAgbWFyZ2luLXRvcDogM3B4O1xyXG59XHJcblxyXG4uZGVzY3JpcHRpb24ge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 6382:
/*!*********************************************!*\
  !*** ./src/app/pages/trips/trips.page.scss ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0cmlwcy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ 1321:
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/trips/trip/trip.component.html ***!
  \********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-item color=\"dark\">\r\n  <ion-icon name=\"location-outline\" color=\"tertiary\"></ion-icon>\r\n  <ion-label\r\n    color=\"light\"\r\n    class=\"ion-margin-start ion-margin-top ion-margin-bottom\"\r\n  >\r\n    <!-- <h2 class=\"title\">{{ item.title }}</h2> -->\r\n    <h2 class=\"title\">Upcoming Reservation</h2>\r\n\r\n    <!-- <p>{{ item.location }}</p> -->\r\n    <p>Abu Dhabi</p>\r\n\r\n    <!-- <p class=\"duration\">{{ item.date_time }}</p> -->\r\n    <p class=\"duration\">Fir, 08 May - Sat, 17 May</p>\r\n\r\n    <!-- <p class=\"description\">{{ item.description }}</p> -->\r\n    <p class=\"description\">Hotel Reservation and Flight booked</p>\r\n  </ion-label>\r\n</ion-item>\r\n");

/***/ }),

/***/ 3673:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/trips/trips.page.html ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<app-header [title]=\"'Trips'\" [showMenu]=\"true\" [back]=\"false\"> </app-header>\r\n\r\n<ion-content color=\"primary\">\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col size=\"12\" *ngFor=\"let item of trips\" class=\"ion-margin-top\">\r\n        <!-- <trip [item]=\"item\"></trip> -->\r\n        <trip></trip>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_trips_trips_module_ts.js.map
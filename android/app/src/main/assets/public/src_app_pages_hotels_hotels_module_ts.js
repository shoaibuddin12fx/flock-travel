(self["webpackChunkVeenme"] = self["webpackChunkVeenme"] || []).push([["src_app_pages_hotels_hotels_module_ts"],{

/***/ 6916:
/*!***************************************************************!*\
  !*** ./src/app/pages/hotels/hotel-row/hotel-row.component.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HotelRowComponent": () => (/* binding */ HotelRowComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_hotel_row_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./hotel-row.component.html */ 9003);
/* harmony import */ var _hotel_row_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./hotel-row.component.scss */ 1200);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);




let HotelRowComponent = class HotelRowComponent {
    constructor() { }
    ngOnInit() { }
};
HotelRowComponent.ctorParameters = () => [];
HotelRowComponent.propDecorators = {
    item: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }]
};
HotelRowComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'hotel-row',
        template: _raw_loader_hotel_row_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_hotel_row_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], HotelRowComponent);



/***/ }),

/***/ 331:
/*!************************************************************!*\
  !*** ./src/app/pages/hotels/hotel-row/hotel-row.module.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HotelRowModule": () => (/* binding */ HotelRowModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _hotel_row_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./hotel-row.component */ 6916);





let HotelRowModule = class HotelRowModule {
};
HotelRowModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [_hotel_row_component__WEBPACK_IMPORTED_MODULE_0__.HotelRowComponent],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.IonicModule],
        exports: [_hotel_row_component__WEBPACK_IMPORTED_MODULE_0__.HotelRowComponent],
    })
], HotelRowModule);



/***/ }),

/***/ 539:
/*!*******************************************************!*\
  !*** ./src/app/pages/hotels/hotels-routing.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HotelsPageRoutingModule": () => (/* binding */ HotelsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _hotels_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./hotels.page */ 8304);




const routes = [
    {
        path: '',
        component: _hotels_page__WEBPACK_IMPORTED_MODULE_0__.HotelsPage
    }
];
let HotelsPageRoutingModule = class HotelsPageRoutingModule {
};
HotelsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], HotelsPageRoutingModule);



/***/ }),

/***/ 7574:
/*!***********************************************!*\
  !*** ./src/app/pages/hotels/hotels.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HotelsPageModule": () => (/* binding */ HotelsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _hotels_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./hotels-routing.module */ 539);
/* harmony import */ var _hotels_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./hotels.page */ 8304);
/* harmony import */ var src_app_components_header_header_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/header/header.module */ 4546);
/* harmony import */ var _hotel_row_hotel_row_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./hotel-row/hotel-row.module */ 331);









let HotelsPageModule = class HotelsPageModule {
};
HotelsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.IonicModule,
            _hotels_routing_module__WEBPACK_IMPORTED_MODULE_0__.HotelsPageRoutingModule,
            src_app_components_header_header_module__WEBPACK_IMPORTED_MODULE_2__.HeaderModule,
            _hotel_row_hotel_row_module__WEBPACK_IMPORTED_MODULE_3__.HotelRowModule,
        ],
        declarations: [_hotels_page__WEBPACK_IMPORTED_MODULE_1__.HotelsPage],
    })
], HotelsPageModule);



/***/ }),

/***/ 8304:
/*!*********************************************!*\
  !*** ./src/app/pages/hotels/hotels.page.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HotelsPage": () => (/* binding */ HotelsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_hotels_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./hotels.page.html */ 8983);
/* harmony import */ var _hotels_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./hotels.page.scss */ 2540);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 4282);





let HotelsPage = class HotelsPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.hotels = [];
    }
    ngOnInit() {
        this.hotels = this.dataService.getHotels();
    }
    goToHotelLocation() {
        this.nav.push('pages/hotel-location');
    }
};
HotelsPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
HotelsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-hotels',
        template: _raw_loader_hotels_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_hotels_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], HotelsPage);



/***/ }),

/***/ 1200:
/*!*****************************************************************!*\
  !*** ./src/app/pages/hotels/hotel-row/hotel-row.component.scss ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-item {\n  --border-radius: 5px;\n}\n\nion-img {\n  width: 140px;\n  height: 130px;\n  object-fit: fill;\n}\n\n.location-div {\n  background-color: var(--ion-color-secondary);\n  height: 40px;\n  width: 40px;\n  border-radius: 20px;\n  position: absolute;\n  left: 120px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvdGVsLXJvdy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG9CQUFBO0FBQ0Y7O0FBRUE7RUFDRSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0FBQ0Y7O0FBRUE7RUFDRSw0Q0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUFDRiIsImZpbGUiOiJob3RlbC1yb3cuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taXRlbSB7XHJcbiAgLS1ib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuXHJcbmlvbi1pbWcge1xyXG4gIHdpZHRoOiAxNDBweDtcclxuICBoZWlnaHQ6IDEzMHB4O1xyXG4gIG9iamVjdC1maXQ6IGZpbGw7XHJcbn1cclxuXHJcbi5sb2NhdGlvbi1kaXYge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xyXG4gIGhlaWdodDogNDBweDtcclxuICB3aWR0aDogNDBweDtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBsZWZ0OiAxMjBweDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ 2540:
/*!***********************************************!*\
  !*** ./src/app/pages/hotels/hotels.page.scss ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".rate {\n  color: var(--ion-color-secondary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvdGVscy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQ0FBQTtBQUNGIiwiZmlsZSI6ImhvdGVscy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucmF0ZSB7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 9003:
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/hotels/hotel-row/hotel-row.component.html ***!
  \*******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-item color=\"dark\" lines=\"none\" class=\"ion-no-padding\">\n  <ion-img [src]=\"item.image\"> </ion-img>\n  <ion-label style=\"margin-left: 40px\">\n    <h2 style=\"font-size: 2.5vh\">{{ item.name }}</h2>\n    <ion-icon\n      *ngFor=\"let n of [1, 2, 3, 4, 5]\"\n      [name]=\"item.rating <= n ? 'star-outline' : 'star'\"\n      [color]=\"item.rating <= n ? 'tertiary' : ''\"\n    ></ion-icon>\n\n    <p\n      style=\"\n        color: var(--ion-color-secondary);\n        font-size: 1.8vh;\n        margin-top: 5px;\n      \"\n    >\n      {{ item.rate }} <ion-text color=\"light\">&nbsp; / &nbsp; </ion-text>\n      <ion-text style=\"color: gray; font-size: 1.3vh; font-weight: bold\">\n        Per Night</ion-text\n      >\n    </p>\n  </ion-label>\n\n  <div class=\"location-div center\">\n    <ion-icon name=\"navigate\"></ion-icon>\n  </div>\n</ion-item>\n");

/***/ }),

/***/ 8983:
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/hotels/hotels.page.html ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<app-header\n  [title]=\"'Hotels In Dubai'\"\n  [searchVisible]=\"false\"\n  [menuVisible]=\"false\"\n  [backVisible]=\"true\"\n>\n</app-header>\n\n<ion-content color=\"primary\">\n  <ion-grid class=\"ion-margin-top ion-padding-top\">\n    <ion-row>\n      <ion-col size=\"12\" *ngFor=\"let item of hotels\">\n        <hotel-row [item]=\"item\" (click)=\"goToHotelLocation()\"></hotel-row>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_hotels_hotels_module_ts.js.map
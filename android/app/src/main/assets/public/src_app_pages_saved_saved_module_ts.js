(self["webpackChunkVeenme"] = self["webpackChunkVeenme"] || []).push([["src_app_pages_saved_saved_module_ts"],{

/***/ 4879:
/*!*****************************************************!*\
  !*** ./src/app/pages/saved/saved-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SavedPageRoutingModule": () => (/* binding */ SavedPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _saved_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./saved.page */ 7872);




const routes = [
    {
        path: '',
        component: _saved_page__WEBPACK_IMPORTED_MODULE_0__.SavedPage
    }
];
let SavedPageRoutingModule = class SavedPageRoutingModule {
};
SavedPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SavedPageRoutingModule);



/***/ }),

/***/ 7239:
/*!*********************************************!*\
  !*** ./src/app/pages/saved/saved.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SavedPageModule": () => (/* binding */ SavedPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _saved_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./saved-routing.module */ 4879);
/* harmony import */ var _saved_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./saved.page */ 7872);
/* harmony import */ var src_app_components_header_header_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/header/header.module */ 4546);








let SavedPageModule = class SavedPageModule {
};
SavedPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _saved_routing_module__WEBPACK_IMPORTED_MODULE_0__.SavedPageRoutingModule,
            src_app_components_header_header_module__WEBPACK_IMPORTED_MODULE_2__.HeaderModule
        ],
        declarations: [_saved_page__WEBPACK_IMPORTED_MODULE_1__.SavedPage]
    })
], SavedPageModule);



/***/ }),

/***/ 7872:
/*!*******************************************!*\
  !*** ./src/app/pages/saved/saved.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SavedPage": () => (/* binding */ SavedPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_saved_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./saved.page.html */ 86);
/* harmony import */ var _saved_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./saved.page.scss */ 72);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 4282);





let SavedPage = class SavedPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
    }
    ngOnInit() {
        this.list = this.dataService.getDestinations();
    }
};
SavedPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
SavedPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-saved',
        template: _raw_loader_saved_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_saved_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SavedPage);



/***/ }),

/***/ 72:
/*!*********************************************!*\
  !*** ./src/app/pages/saved/saved.page.scss ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-img {\n  height: 200px;\n  object-fit: fill;\n}\n\nion-img::part(image) {\n  border-radius: 15px;\n}\n\nion-label {\n  position: absolute;\n  text-align: center;\n  width: 100%;\n  height: 100%;\n  bottom: 25%;\n  font-size: 2.2vh;\n  font-weight: bold;\n}\n\n.gradient {\n  position: absolute;\n  text-align: center;\n  width: 96%;\n  height: 96%;\n  bottom: 2%;\n  font-size: 2.2vh;\n  font-weight: bold;\n  border-radius: 15px;\n  background-image: linear-gradient(to bottom, rgba(229, 93, 135, 0.25), rgba(95, 195, 228, 0.25));\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNhdmVkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7RUFDQSxnQkFBQTtBQUNGOztBQUVBO0VBQ0UsbUJBQUE7QUFDRjs7QUFFQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0dBQUE7QUFDRiIsImZpbGUiOiJzYXZlZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taW1nIHtcclxuICBoZWlnaHQ6IDIwMHB4O1xyXG4gIG9iamVjdC1maXQ6IGZpbGw7XHJcbn1cclxuXHJcbmlvbi1pbWc6OnBhcnQoaW1hZ2UpIHtcclxuICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG59XHJcblxyXG5pb24tbGFiZWwge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIGJvdHRvbTogMjUlO1xyXG4gIGZvbnQtc2l6ZTogMi4ydmg7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbi5ncmFkaWVudCB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB3aWR0aDogOTYlO1xyXG4gIGhlaWdodDogOTYlO1xyXG4gIGJvdHRvbTogMiU7XHJcbiAgZm9udC1zaXplOiAyLjJ2aDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudChcclxuICAgIHRvIGJvdHRvbSxcclxuICAgIHJnYmEoMjI5LCA5MywgMTM1LCAwLjI1KSxcclxuICAgIHJnYmEoOTUsIDE5NSwgMjI4LCAwLjI1KVxyXG4gICk7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 86:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/saved/saved.page.html ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<app-header [title]=\"'Favourites'\"> </app-header>\r\n\r\n<ion-content color=\"primary\">\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col size=\"6\" *ngFor=\"let item of list\">\r\n        <div>\r\n          <ion-img [src]=\"item.image\"></ion-img>\r\n          <div class=\"gradient\"></div>\r\n          <ion-label>{{item.name}}</ion-label>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_saved_saved_module_ts.js.map
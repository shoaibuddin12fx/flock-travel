(self["webpackChunkVeenme"] = self["webpackChunkVeenme"] || []).push([["src_app_pages_destination_destination_module_ts"],{

/***/ 7565:
/*!****************************************************************************************!*\
  !*** ./src/app/components/destinations/destination-item/destination-item.component.ts ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DestinationItemComponent": () => (/* binding */ DestinationItemComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_destination_item_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./destination-item.component.html */ 6773);
/* harmony import */ var _destination_item_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./destination-item.component.scss */ 5046);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);




let DestinationItemComponent = class DestinationItemComponent {
    constructor() { }
    ngOnInit() { }
};
DestinationItemComponent.ctorParameters = () => [];
DestinationItemComponent.propDecorators = {
    item: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }]
};
DestinationItemComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'destination-item',
        template: _raw_loader_destination_item_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_destination_item_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], DestinationItemComponent);



/***/ }),

/***/ 3856:
/*!*************************************************************************************!*\
  !*** ./src/app/components/destinations/destination-item/destination-item.module.ts ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DestinationItemModule": () => (/* binding */ DestinationItemModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _destination_item_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./destination-item.component */ 7565);





let DestinationItemModule = class DestinationItemModule {
};
DestinationItemModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [_destination_item_component__WEBPACK_IMPORTED_MODULE_0__.DestinationItemComponent],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.IonicModule],
        exports: [_destination_item_component__WEBPACK_IMPORTED_MODULE_0__.DestinationItemComponent],
    })
], DestinationItemModule);



/***/ }),

/***/ 9664:
/*!*****************************************************************!*\
  !*** ./src/app/pages/destination/destination-routing.module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DestinationPageRoutingModule": () => (/* binding */ DestinationPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _destination_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./destination.page */ 300);




const routes = [
    {
        path: '',
        component: _destination_page__WEBPACK_IMPORTED_MODULE_0__.DestinationPage
    }
];
let DestinationPageRoutingModule = class DestinationPageRoutingModule {
};
DestinationPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], DestinationPageRoutingModule);



/***/ }),

/***/ 9528:
/*!*********************************************************!*\
  !*** ./src/app/pages/destination/destination.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DestinationPageModule": () => (/* binding */ DestinationPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _destination_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./destination-routing.module */ 9664);
/* harmony import */ var _destination_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./destination.page */ 300);
/* harmony import */ var src_app_components_destinations_destination_item_destination_item_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/destinations/destination-item/destination-item.module */ 3856);








let DestinationPageModule = class DestinationPageModule {
};
DestinationPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _destination_routing_module__WEBPACK_IMPORTED_MODULE_0__.DestinationPageRoutingModule,
            src_app_components_destinations_destination_item_destination_item_module__WEBPACK_IMPORTED_MODULE_2__.DestinationItemModule,
        ],
        declarations: [_destination_page__WEBPACK_IMPORTED_MODULE_1__.DestinationPage],
    })
], DestinationPageModule);



/***/ }),

/***/ 5046:
/*!******************************************************************************************!*\
  !*** ./src/app/components/destinations/destination-item/destination-item.component.scss ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-img {\n  height: 140px;\n  width: 100%;\n  object-fit: fill;\n}\n\n.color {\n  background-color: black;\n}\n\n.cardBorder {\n  border-radius: 15px;\n}\n\n.margin-top {\n  margin-top: 20px;\n}\n\n.fontStyle {\n  font-size: 14px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRlc3RpbmF0aW9uLWl0ZW0uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FBQ0Y7O0FBQ0E7RUFDRSx1QkFBQTtBQUVGOztBQUFBO0VBQ0UsbUJBQUE7QUFHRjs7QUFEQTtFQUNFLGdCQUFBO0FBSUY7O0FBRkE7RUFDRSxlQUFBO0FBS0YiLCJmaWxlIjoiZGVzdGluYXRpb24taXRlbS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1pbWcge1xyXG4gIGhlaWdodDogMTQwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgb2JqZWN0LWZpdDogZmlsbDtcclxufVxyXG4uY29sb3Ige1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG59XHJcbi5jYXJkQm9yZGVyIHtcclxuICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG59XHJcbi5tYXJnaW4tdG9wIHtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG59XHJcbi5mb250U3R5bGUge1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ 6773:
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/destinations/destination-item/destination-item.component.html ***!
  \********************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-card class=\"ion-no-margin ion-margin-start margin-top cardBorder\">\n  <ion-img [src]=\"item.image\"></ion-img>\n  <ion-card-content class=\"ion-no-padding\">\n    <ion-item lines=\"none\" class=\"ion-no-padding\" color=\"primary\">\n      <ion-card-subtitle color=\"light\" class=\"m-l-10 fontStyle\">{{\n        item.name\n      }}</ion-card-subtitle>\n      <ion-icon\n        *ngIf=\"item.selected\"\n        name=\"checkmark-circle-outline\"\n        slot=\"end\"\n        color=\"secondary\"\n      ></ion-icon>\n    </ion-item>\n  </ion-card-content>\n</ion-card>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_destination_destination_module_ts.js.map
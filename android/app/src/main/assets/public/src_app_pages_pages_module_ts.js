(self["webpackChunkVeenme"] = self["webpackChunkVeenme"] || []).push([["src_app_pages_pages_module_ts"],{

/***/ 9730:
/*!***********************************************!*\
  !*** ./src/app/pages/pages-routing.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesRoutingModule": () => (/* binding */ PagesRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 9895);



const routes = [
    {
        path: '',
        redirectTo: 'splash',
        pathMatch: 'full',
    },
    {
        path: 'splash',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_splash_splash_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./splash/splash.module */ 1203)).then((m) => m.SplashPageModule),
    },
    {
        path: 'signup',
        loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(__webpack_require__, /*! ./signup/signup.module */ 7110)).then((m) => m.SignupPageModule),
    },
    {
        path: 'login',
        loadChildren: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(__webpack_require__, /*! ./login/login.module */ 1053)).then((m) => m.LoginPageModule),
    },
    // {
    //   path: 'home',
    //   loadChildren: () =>
    //     import('./home/home.module').then((m) => m.HomePageModule),
    // },
    {
        path: 'tabbar',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_tabbar_tabbar_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./tabbar/tabbar.module */ 9920)).then((m) => m.TabbarPageModule),
    },
    {
        path: 'notifications',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_notifications_notifications_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./notifications/notifications.module */ 4558)).then((m) => m.NotificationsPageModule),
    },
    {
        path: 'edit-profile',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_edit-profile_edit-profile_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./edit-profile/edit-profile.module */ 483)).then((m) => m.EditProfilePageModule),
    },
    {
        path: 'destination',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_destination_destination_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./destination/destination.module */ 9528)).then((m) => m.DestinationPageModule),
    },
    {
        path: 'book-initial',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_book-initial_book-initial_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./book-initial/book-initial.module */ 9102)).then((m) => m.BookInitialPageModule),
    },
    {
        path: 'trips',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_trips_trips_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./trips/trips.module */ 4629)).then((m) => m.TripsPageModule),
    },
    {
        path: 'hotels',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_hotels_hotels_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./hotels/hotels.module */ 7574)).then(m => m.HotelsPageModule)
    },
    {
        path: 'hotel-location',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_hotel-location_hotel-location_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./hotel-location/hotel-location.module */ 9860)).then(m => m.HotelLocationPageModule)
    },
    {
        path: 'hotel-detail',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_hotel-detail_hotel-detail_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./hotel-detail/hotel-detail.module */ 8825)).then(m => m.HotelDetailPageModule)
    },
    {
        path: 'search-flights',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_search-flights_search-flights_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./search-flights/search-flights.module */ 1455)).then(m => m.SearchFlightsPageModule)
    },
    {
        path: 'flights',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_flights_flights_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./flights/flights.module */ 6244)).then(m => m.FlightsPageModule)
    },
    {
        path: 'flights-detail',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_flights-detail_flights-detail_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./flights-detail/flights-detail.module */ 3704)).then(m => m.FlightsDetailPageModule)
    },
    {
        path: 'payment-methods',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_payment-methods_payment-methods_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./payment-methods/payment-methods.module */ 6849)).then(m => m.PaymentMethodsPageModule)
    },
    {
        path: 'search-destination',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_search-destination_search-destination_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./search-destination/search-destination.module */ 6040)).then(m => m.SearchDestinationPageModule)
    },
    {
        path: 'current-location',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_current-location_current-location_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./current-location/current-location.module */ 8443)).then(m => m.CurrentLocationPageModule)
    },
];
let PagesRoutingModule = class PagesRoutingModule {
};
PagesRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule],
    })
], PagesRoutingModule);



/***/ }),

/***/ 8950:
/*!***************************************!*\
  !*** ./src/app/pages/pages.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesModule": () => (/* binding */ PagesModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _pages_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages-routing.module */ 9730);






let PagesModule = class PagesModule {
};
PagesModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _pages_routing_module__WEBPACK_IMPORTED_MODULE_0__.PagesRoutingModule,
        ],
    })
], PagesModule);



/***/ })

}]);
//# sourceMappingURL=src_app_pages_pages_module_ts.js.map
(self["webpackChunkVeenme"] = self["webpackChunkVeenme"] || []).push([["src_app_pages_book-initial_book-initial_module_ts"],{

/***/ 4604:
/*!*******************************************************************!*\
  !*** ./src/app/pages/book-initial/book-initial-routing.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookInitialPageRoutingModule": () => (/* binding */ BookInitialPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _book_initial_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./book-initial.page */ 7607);




const routes = [
    {
        path: '',
        component: _book_initial_page__WEBPACK_IMPORTED_MODULE_0__.BookInitialPage
    }
];
let BookInitialPageRoutingModule = class BookInitialPageRoutingModule {
};
BookInitialPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], BookInitialPageRoutingModule);



/***/ }),

/***/ 9102:
/*!***********************************************************!*\
  !*** ./src/app/pages/book-initial/book-initial.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookInitialPageModule": () => (/* binding */ BookInitialPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _book_initial_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./book-initial-routing.module */ 4604);
/* harmony import */ var _book_initial_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./book-initial.page */ 7607);







let BookInitialPageModule = class BookInitialPageModule {
};
BookInitialPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _book_initial_routing_module__WEBPACK_IMPORTED_MODULE_0__.BookInitialPageRoutingModule
        ],
        declarations: [_book_initial_page__WEBPACK_IMPORTED_MODULE_1__.BookInitialPage]
    })
], BookInitialPageModule);



/***/ }),

/***/ 7607:
/*!*********************************************************!*\
  !*** ./src/app/pages/book-initial/book-initial.page.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookInitialPage": () => (/* binding */ BookInitialPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_book_initial_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./book-initial.page.html */ 4527);
/* harmony import */ var _book_initial_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./book-initial.page.scss */ 895);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var src_app_services_basic_nav_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/basic/nav.service */ 6350);





let BookInitialPage = class BookInitialPage {
    constructor(nav) {
        this.nav = nav;
        this.step = 1;
        this.person_count = 1;
    }
    ngOnInit() { }
    back() {
        if (this.step === 1)
            this.nav.pop();
        else
            this.step--;
    }
    next() {
        this.step++;
    }
    addRemovePerson(isPlus) {
        if (isPlus)
            this.person_count++;
        else if (this.person_count > 1)
            this.person_count--;
    }
    goToHotels() {
        this.nav.push('pages/hotels');
    }
};
BookInitialPage.ctorParameters = () => [
    { type: src_app_services_basic_nav_service__WEBPACK_IMPORTED_MODULE_2__.NavService }
];
BookInitialPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-book-initial',
        template: _raw_loader_book_initial_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_book_initial_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], BookInitialPage);



/***/ }),

/***/ 895:
/*!***********************************************************!*\
  !*** ./src/app/pages/book-initial/book-initial.page.scss ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".bg {\n  width: 100%;\n  height: 100vh;\n  background: url('tower2-bg.jpeg') !important;\n  background-repeat: no-repeat !important;\n  background-size: cover !important;\n  background-position: center !important;\n}\n\n.overlay {\n  width: 100%;\n  height: 100vh;\n  background: rgba(3, 3, 3, 0.6);\n}\n\n.gradient {\n  position: absolute;\n  text-align: center;\n  width: 96%;\n  height: 96%;\n  bottom: 2%;\n  font-size: 2.2vh;\n  font-weight: bold;\n  border-radius: 15px;\n  background-image: linear-gradient(to bottom, rgba(229, 93, 135, 0.25), rgba(95, 195, 228, 0.25));\n}\n\n.book-btn {\n  width: 100px;\n  height: 37px;\n  --border-radius: 5px;\n}\n\n.back-btn {\n  position: absolute;\n  top: 4%;\n  left: 2%;\n}\n\n.side-icons {\n  position: absolute;\n  top: 4%;\n  right: 20px;\n}\n\n.icon-div {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  flex-wrap: wrap;\n  font-size: 1.2vh;\n}\n\n.icon {\n  margin: 10%;\n  width: 30px;\n  height: 30px;\n  background-color: #1d1c1c;\n  border-radius: 10px;\n  color: white;\n  padding: 10px;\n  font-size: 1vh;\n}\n\n.date-time-div {\n  width: 90%;\n  position: absolute;\n  bottom: 3%;\n  left: 5%;\n  right: 5%;\n}\n\n.date-time-div .dt-internal-div {\n  background-color: red;\n  border: 1.5px solid var(--ion-color-secondary);\n  border-radius: 20px;\n  background-color: var(--ion-color-dark);\n  padding: 20px;\n}\n\n.date-time {\n  background-color: white;\n  border-radius: 5px;\n  color: #0a0a0b;\n}\n\n.add-remov-btn {\n  width: 45px;\n  height: 40px;\n}\n\n.persons-count-div {\n  background-color: var(--ion-color-secondary);\n  width: 45px;\n  height: 40px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border-radius: 5px;\n  margin-right: 10px;\n  margin-left: 10px;\n}\n\n.info-div {\n  width: 70%;\n  height: 25vh;\n  border: 1.5px solid var(--ion-color-secondary);\n  border-radius: 20px;\n  background-color: var(--ion-color-dark);\n  position: absolute;\n  bottom: 15%;\n  left: 15%;\n  right: 15%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-wrap: wrap;\n  flex-direction: row;\n}\n\n.info-div h2 {\n  color: white;\n}\n\n.info-div p {\n  margin: -5% 0 0 0;\n  text-align: center;\n  color: white;\n  font-size: 12px;\n  width: 80%;\n  line-height: 1.5;\n}\n\n.info-div ion-button {\n  border-radius: 20px !important;\n  margin: 0 0 8% 0 !important;\n  width: 80%;\n}\n\n.swip-pera {\n  position: absolute;\n  bottom: 2%;\n  left: 35%;\n  color: rgba(255, 255, 255, 0.6);\n  font-size: 10px;\n  border-bottom: 4px solid rgba(255, 255, 255, 0.7);\n  border-radius: 5px;\n}\n\n.under-line {\n  width: 25%;\n  border-radius: 20px;\n  height: 3px;\n  background-color: white;\n  position: absolute;\n  bottom: 2.5%;\n  left: 38.5%;\n}\n\n.center-div {\n  width: 75%;\n  position: absolute;\n  bottom: 0;\n  left: 0%;\n}\n\n.center-div h2 {\n  margin: 0 0 0 2%;\n  color: white;\n}\n\n.center-div p {\n  margin: 0 0 2% 2%;\n  color: white;\n  text-align: start;\n  width: 80%;\n  font-size: 12px;\n  line-height: 2;\n}\n\n.center-div ion-button {\n  margin: 0 0 8% 2%;\n  width: 50%;\n}\n\n.center-div .bottom-div {\n  width: 100vw;\n  height: 50vh;\n  background-color: var(--ion-color-primary);\n}\n\n.center-div .bottom-div h2 {\n  position: relative;\n  top: 5%;\n  margin: 10% 0 0 3%;\n}\n\n.center-div .bottom-div .des-pera {\n  margin: 5% 0 0 3%;\n  color: #888888;\n}\n\n.center-div .bottom-div .modal-img {\n  margin: 8% 0 0 0;\n  display: flex;\n  flex-direction: row;\n  flex-wrap: nowrap;\n}\n\n.center-div .bottom-div .modal-img img {\n  width: 40%;\n  margin: 0 3%;\n  height: 200px;\n  border-radius: 20px;\n}\n\n.center-div .scrolling-wrapper {\n  overflow-x: scroll;\n  overflow-y: hidden;\n  white-space: nowrap;\n}\n\n.center-div .scrolling-wrapper .card {\n  display: inline-block;\n  width: 70%;\n}\n\n.center-div .scrolling-wrapper .item {\n  display: inline-block;\n  width: 40%;\n}\n\n.center-div .icon-size {\n  font-size: 17px;\n  margin-top: 20px;\n}\n\n.center-div .img-des {\n  position: absolute;\n  bottom: 40px;\n  left: 60px;\n  width: 90px;\n  color: white;\n}\n\n.center-div .img-des p {\n  font-size: 12px;\n  margin: -10% 0 0 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJvb2staW5pdGlhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLDRDQUFBO0VBQ0EsdUNBQUE7RUFDQSxpQ0FBQTtFQUNBLHNDQUFBO0FBQ0Y7O0FBRUE7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0FBQ0Y7O0FBU0E7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0dBQUE7QUFORjs7QUFhQTtFQUNFLFlBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7QUFWRjs7QUFhQTtFQUNFLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7QUFWRjs7QUFZQTtFQUNFLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7QUFURjs7QUFZQTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFURjs7QUFXQTtFQUNFLFdBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUFSRjs7QUFXQTtFQUNFLFVBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtBQVJGOztBQVVFO0VBQ0UscUJBQUE7RUFDQSw4Q0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUNBQUE7RUFDQSxhQUFBO0FBUko7O0FBWUE7RUFDRSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQVRGOztBQVlBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUFURjs7QUFZQTtFQUNFLDRDQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQVRGOztBQVlBO0VBQ0UsVUFBQTtFQUNBLFlBQUE7RUFDQSw4Q0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FBVEY7O0FBV0U7RUFDRSxZQUFBO0FBVEo7O0FBV0U7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUFUSjs7QUFZRTtFQUNFLDhCQUFBO0VBQ0EsMkJBQUE7RUFDQSxVQUFBO0FBVko7O0FBY0E7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsK0JBQUE7RUFDQSxlQUFBO0VBQ0EsaURBQUE7RUFDQSxrQkFBQTtBQVhGOztBQWFBO0VBQ0UsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQVZGOztBQWNBO0VBQ0UsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7QUFYRjs7QUFhRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtBQVhKOztBQWFFO0VBQ0UsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFYSjs7QUFhRTtFQUNFLGlCQUFBO0VBQ0EsVUFBQTtBQVhKOztBQWNFO0VBQ0UsWUFBQTtFQUNBLFlBQUE7RUFDQSwwQ0FBQTtBQVpKOztBQWNJO0VBQ0Usa0JBQUE7RUFDQSxPQUFBO0VBQ0Esa0JBQUE7QUFaTjs7QUFjSTtFQUNFLGlCQUFBO0VBQ0EsY0FBQTtBQVpOOztBQWNJO0VBQ0UsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQVpOOztBQWNNO0VBQ0UsVUFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUFaUjs7QUFpQkU7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUFmSjs7QUFpQkk7RUFDRSxxQkFBQTtFQUNBLFVBQUE7QUFmTjs7QUFtQkk7RUFDRSxxQkFBQTtFQUNBLFVBQUE7QUFqQk47O0FBc0JFO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0FBcEJKOztBQXNCRTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQXBCSjs7QUFzQkk7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QUFwQk4iLCJmaWxlIjoiYm9vay1pbml0aWFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDB2aDtcclxuICBiYWNrZ3JvdW5kOiB1cmwoXCIuLi8uLi8uLi9hc3NldHMvSW1hZ2VzL3Rvd2VyMi1iZy5qcGVnXCIpICFpbXBvcnRhbnQ7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdCAhaW1wb3J0YW50O1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXIgIWltcG9ydGFudDtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm92ZXJsYXkge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwdmg7XHJcbiAgYmFja2dyb3VuZDogcmdiYSgzLCAzLCAzLCAwLjYpO1xyXG5cclxuICAvLyAgIHJhZGlhbC1ncmFkaWVudChcclxuICAvLyAgICAgY2lyY2xlLFxyXG4gIC8vICAgICByZ2JhKDMsIDMsIDMsIDAuNDg0KSAzMCUsXHJcbiAgLy8gICAgIHJnYmEoMCwgMCwgMCwgMC41NzMpIDM1JSxcclxuICAvLyAgICAgcmdiYSgwLCAwLCAwLCAwLjM3NCkgMTAwJVxyXG4gIC8vICAgKTtcclxufVxyXG5cclxuLmdyYWRpZW50IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHdpZHRoOiA5NiU7XHJcbiAgaGVpZ2h0OiA5NiU7XHJcbiAgYm90dG9tOiAyJTtcclxuICBmb250LXNpemU6IDIuMnZoO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KFxyXG4gICAgdG8gYm90dG9tLFxyXG4gICAgcmdiYSgyMjksIDkzLCAxMzUsIDAuMjUpLFxyXG4gICAgcmdiYSg5NSwgMTk1LCAyMjgsIDAuMjUpXHJcbiAgKTtcclxufVxyXG5cclxuLmJvb2stYnRuIHtcclxuICB3aWR0aDogMTAwcHg7XHJcbiAgaGVpZ2h0OiAzN3B4O1xyXG4gIC0tYm9yZGVyLXJhZGl1czogNXB4O1xyXG59XHJcblxyXG4uYmFjay1idG4ge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDQlO1xyXG4gIGxlZnQ6IDIlO1xyXG59XHJcbi5zaWRlLWljb25zIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiA0JTtcclxuICByaWdodDogMjBweDtcclxufVxyXG5cclxuLmljb24tZGl2IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgZm9udC1zaXplOiAxLjJ2aDtcclxufVxyXG4uaWNvbiB7XHJcbiAgbWFyZ2luOiAxMCU7XHJcbiAgd2lkdGg6IDMwcHg7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyOSwgMjgsIDI4KTtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIGZvbnQtc2l6ZTogMXZoO1xyXG59XHJcblxyXG4uZGF0ZS10aW1lLWRpdiB7XHJcbiAgd2lkdGg6IDkwJTtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgYm90dG9tOiAzJTtcclxuICBsZWZ0OiA1JTtcclxuICByaWdodDogNSU7XHJcblxyXG4gIC5kdC1pbnRlcm5hbC1kaXYge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmVkO1xyXG4gICAgYm9yZGVyOiAxLjVweCBzb2xpZCB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyayk7XHJcbiAgICBwYWRkaW5nOiAyMHB4O1xyXG4gIH1cclxufVxyXG5cclxuLmRhdGUtdGltZSB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIGNvbG9yOiAjMGEwYTBiO1xyXG59XHJcblxyXG4uYWRkLXJlbW92LWJ0biB7XHJcbiAgd2lkdGg6IDQ1cHg7XHJcbiAgaGVpZ2h0OiA0MHB4O1xyXG59XHJcblxyXG4ucGVyc29ucy1jb3VudC1kaXYge1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xyXG4gIHdpZHRoOiA0NXB4O1xyXG4gIGhlaWdodDogNDBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIG1hcmdpbi1yaWdodDogMTBweDtcclxuICBtYXJnaW4tbGVmdDogMTBweDtcclxufVxyXG5cclxuLmluZm8tZGl2IHtcclxuICB3aWR0aDogNzAlO1xyXG4gIGhlaWdodDogMjV2aDtcclxuICBib3JkZXI6IDEuNXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmspO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBib3R0b206IDE1JTtcclxuICBsZWZ0OiAxNSU7XHJcbiAgcmlnaHQ6IDE1JTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgZmxleC13cmFwOiB3cmFwO1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcblxyXG4gIGgyIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICB9XHJcbiAgcCB7XHJcbiAgICBtYXJnaW46IC01JSAwIDAgMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gIH1cclxuXHJcbiAgaW9uLWJ1dHRvbiB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBtYXJnaW46IDAgMCA4JSAwICFpbXBvcnRhbnQ7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gIH1cclxufVxyXG5cclxuLnN3aXAtcGVyYSB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJvdHRvbTogMiU7XHJcbiAgbGVmdDogMzUlO1xyXG4gIGNvbG9yOiByZ2JhKCRjb2xvcjogI2ZmZiwgJGFscGhhOiAwLjYpO1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxuICBib3JkZXItYm90dG9tOiA0cHggc29saWQgcmdiYSgkY29sb3I6ICNmZmYsICRhbHBoYTogMC43KTtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuLnVuZGVyLWxpbmUge1xyXG4gIHdpZHRoOiAyNSU7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICBoZWlnaHQ6IDNweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJvdHRvbTogMi41JTtcclxuICBsZWZ0OiAzOC41JTtcclxufVxyXG5cclxuLy8gU2Vjb25kIFNjcmVlbiBzdGFydFxyXG4uY2VudGVyLWRpdiB7XHJcbiAgd2lkdGg6IDc1JTtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGxlZnQ6IDAlO1xyXG5cclxuICBoMiB7XHJcbiAgICBtYXJnaW46IDAgMCAwIDIlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH1cclxuICBwIHtcclxuICAgIG1hcmdpbjogMCAwIDIlIDIlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgdGV4dC1hbGlnbjogc3RhcnQ7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDI7XHJcbiAgfVxyXG4gIGlvbi1idXR0b24ge1xyXG4gICAgbWFyZ2luOiAwIDAgOCUgMiU7XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gIH1cclxuXHJcbiAgLmJvdHRvbS1kaXYge1xyXG4gICAgd2lkdGg6IDEwMHZ3O1xyXG4gICAgaGVpZ2h0OiA1MHZoO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG5cclxuICAgIGgyIHtcclxuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICB0b3A6IDUlO1xyXG4gICAgICBtYXJnaW46IDEwJSAwIDAgMyU7XHJcbiAgICB9XHJcbiAgICAuZGVzLXBlcmEge1xyXG4gICAgICBtYXJnaW46IDUlIDAgMCAzJTtcclxuICAgICAgY29sb3I6ICM4ODg4ODg7XHJcbiAgICB9XHJcbiAgICAubW9kYWwtaW1nIHtcclxuICAgICAgbWFyZ2luOiA4JSAwIDAgMDtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgZmxleC13cmFwOiBub3dyYXA7XHJcblxyXG4gICAgICBpbWcge1xyXG4gICAgICAgIHdpZHRoOiA0MCU7XHJcbiAgICAgICAgbWFyZ2luOiAwIDMlO1xyXG4gICAgICAgIGhlaWdodDogMjAwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLnNjcm9sbGluZy13cmFwcGVyIHtcclxuICAgIG92ZXJmbG93LXg6IHNjcm9sbDtcclxuICAgIG92ZXJmbG93LXk6IGhpZGRlbjtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAvLyBtYXJnaW4tdG9wOiAtMTBweDtcclxuICAgIC5jYXJkIHtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICB3aWR0aDogNzAlO1xyXG4gICAgICAvLyBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIH1cclxuXHJcbiAgICAuaXRlbSB7XHJcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgd2lkdGg6IDQwJTtcclxuICAgICAgLy8gbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuaWNvbi1zaXplIHtcclxuICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgfVxyXG4gIC5pbWctZGVzIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJvdHRvbTogNDBweDtcclxuICAgIGxlZnQ6IDYwcHg7XHJcbiAgICB3aWR0aDogOTBweDtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuXHJcbiAgICBwIHtcclxuICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICBtYXJnaW46IC0xMCUgMCAwIDA7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbi8vIFNlY29uZCBTY3JlZW4gRW5kXHJcbiJdfQ== */");

/***/ }),

/***/ 4527:
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/book-initial/book-initial.page.html ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content color=\"primary\">\r\n  <div class=\"bg\">\r\n    <div class=\"overlay\">\r\n      <div class=\"back-btn\" (click)=\"back()\">\r\n        <ion-icon\r\n          size=\"large\"\r\n          class=\"icon\"\r\n          name=\"chevron-back-outline\"\r\n        ></ion-icon>\r\n      </div>\r\n\r\n      <div class=\"side-icons\">\r\n        <div class=\"icon-div\">\r\n          <ion-icon\r\n            size=\"large\"\r\n            class=\"icon\"\r\n            name=\"ellipsis-horizontal-outline\"\r\n          ></ion-icon>\r\n        </div>\r\n        <div class=\"icon-div\" [hidden]=\"step != 1\">\r\n          <ion-icon\r\n            size=\"large\"\r\n            class=\"icon\"\r\n            name=\"person-add-outline\"\r\n          ></ion-icon>\r\n          Invite\r\n        </div>\r\n        <div class=\"icon-div\" [hidden]=\"step != 1\" style=\"margin-top: 10px\">\r\n          <ion-icon size=\"large\" class=\"icon\" name=\"heart-outline\"></ion-icon>\r\n          573\r\n        </div>\r\n        <div class=\"icon-div\" [hidden]=\"step != 1\" style=\"margin-top: 10px\">\r\n          <ion-icon\r\n            size=\"large\"\r\n            class=\"icon\"\r\n            name=\"chatbox-ellipses-outline\"\r\n          ></ion-icon>\r\n          126\r\n        </div>\r\n      </div>\r\n      <div class=\"info-div\" [hidden]=\"step != 1\">\r\n        <h2>Dubai</h2>\r\n        <p style=\"font-size: 1.3vh\">\r\n          The city setting is stunning with a wonderful architecure and\r\n          historical sights and heritage setting.\r\n        </p>\r\n        <ion-button color=\"secondary\" expand=\"block\" (click)=\"next()\"\r\n          >Book a trip</ion-button\r\n        >\r\n      </div>\r\n\r\n      <!-- Step 3 -->\r\n      <div [hidden]=\"step != 3\" class=\"date-time-div\">\r\n        <h2>Dubai</h2>\r\n        <div class=\"dt-internal-div\">\r\n          <p style=\"font-size: 1.3vh; margin-left: 5px\">Select date and time</p>\r\n          <ion-grid>\r\n            <ion-row>\r\n              <ion-col size=\"7\" class=\"ion-no-padding\">\r\n                <div class=\"date-time\">\r\n                  <ion-datetime value=\"2022-01-27\"></ion-datetime>\r\n                </div>\r\n              </ion-col>\r\n\r\n              <ion-col size=\"4.3\" class=\"ion-no-padding ion-margin-start\">\r\n                <div class=\"date-time\">\r\n                  <ion-datetime\r\n                    value=\"2022-01-27\"\r\n                    presentation=\"time\"\r\n                  ></ion-datetime>\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-grid>\r\n\r\n          <p style=\"font-size: 1.3vh; margin-left: 5px\">How many people?</p>\r\n\r\n          <ion-item lines=\"none\" color=\"dark\" class=\"ion-no-padding\">\r\n            <ion-button\r\n              color=\"light\"\r\n              class=\"add-remov-btn\"\r\n              (click)=\"addRemovePerson(false)\"\r\n            >\r\n              <ion-icon name=\"remove-outline\"></ion-icon>\r\n            </ion-button>\r\n\r\n            <div class=\"persons-count-div\">{{person_count}}</div>\r\n\r\n            <ion-button\r\n              color=\"light\"\r\n              class=\"add-remov-btn\"\r\n              (click)=\"addRemovePerson(true)\"\r\n            >\r\n              <ion-icon name=\"add-outline\"></ion-icon>\r\n            </ion-button>\r\n          </ion-item>\r\n\r\n          <ion-button\r\n            color=\"secondary\"\r\n            expand=\"block\"\r\n            (click)=\"goToHotels()\"\r\n            class=\"ion-margin-top\"\r\n            >Check Availability</ion-button\r\n          >\r\n        </div>\r\n      </div>\r\n      <div [hidden]=\"step != 1\" class=\"swip-pera\">\r\n        <p>SWIPE FOR DETAILS</p>\r\n      </div>\r\n\r\n      <!-- <div class=\"under-line\" [hidden]=\"step != 1\"></div> -->\r\n\r\n      <div class=\"center-div\" [hidden]=\"step != 2\">\r\n        <div style=\"margin-left: 2%\">\r\n          <h2>Dubai</h2>\r\n          <p style=\"font-size: 1.2vh; margin-top: 10px\">\r\n            The city setting is stunning with a wonderful architecure and\r\n            historical sights and heritage setting.\r\n          </p>\r\n          <ion-button\r\n            color=\"secondary\"\r\n            expand=\"block\"\r\n            class=\"book-btn\"\r\n            (click)=\"next()\"\r\n            >Book</ion-button\r\n          >\r\n        </div>\r\n        <div class=\"bottom-div\">\r\n          <h2>Popular destinations</h2>\r\n          <p class=\"des-pera\">Most popular Abu Dhabi-area attractions</p>\r\n          <ion-row>\r\n            <div class=\"scrolling-wrapper\">\r\n              <div\r\n                style=\"\r\n                  width: 45%;\r\n                  position: relative;\r\n                  border-radius: 15px;\r\n                  margin-top: 30px;\r\n                  position: relative;\r\n                \"\r\n              >\r\n                <ion-col *ngFor=\"let item of [1,2,3,4]\">\r\n                  <img\r\n                    style=\"\r\n                      width: 150px;\r\n                      height: 220px;\r\n                      border-radius: 20px;\r\n                      object-fit: cover;\r\n                      margin: 0 3%;\r\n                      object-position: center;\r\n                    \"\r\n                    src=\"../../../assets/Images/tower1-bg.jpg\"\r\n                  />\r\n                  <div class=\"img-des\">\r\n                    <p>Abu Dhabi Grand</p>\r\n                    <p>Building Towers</p>\r\n                  </div>\r\n                  <ion-row style=\"position: absolute; bottom: 0; left: 15px\">\r\n                  </ion-row>\r\n                </ion-col>\r\n              </div>\r\n            </div>\r\n          </ion-row>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_book-initial_book-initial_module_ts.js.map
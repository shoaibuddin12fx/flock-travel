(self["webpackChunkVeenme"] = self["webpackChunkVeenme"] || []).push([["src_app_pages_explore_explore_module_ts"],{

/***/ 8087:
/*!***************************************************************************!*\
  !*** ./src/app/components/current-location/current-location.component.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CurrentLocationComponent": () => (/* binding */ CurrentLocationComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_current_location_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./current-location.component.html */ 4030);
/* harmony import */ var _current_location_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./current-location.component.scss */ 5665);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var src_app_services_basic_modal_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/basic/modal.service */ 4307);
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/google-maps */ 7385);






let CurrentLocationComponent = class CurrentLocationComponent {
    constructor(modals, _googleMaps) {
        this.modals = modals;
        this._googleMaps = _googleMaps;
    }
    ngOnInit() { }
    ngAfterViewInit() {
        this.initMap();
    }
    initMap() {
        let element = this.mapElement.nativeElement;
        let loc = new _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__.LatLng(40.7128, -74.0059);
        this.map = this._googleMaps.create(element, { styles: [] });
        this.map.one(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__.GoogleMapsEvent.MAP_READY).then(() => {
            this.moveCamera(loc);
        });
    }
    moveCamera(loc) {
        let options = {
            target: loc,
            zoom: 15,
            tilt: 10
        };
        this.map.moveCamera(options);
    }
};
CurrentLocationComponent.ctorParameters = () => [
    { type: src_app_services_basic_modal_service__WEBPACK_IMPORTED_MODULE_2__.ModalService },
    { type: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_3__.GoogleMaps }
];
CurrentLocationComponent.propDecorators = {
    mapElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.ViewChild, args: ['map',] }]
};
CurrentLocationComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-current-location',
        template: _raw_loader_current_location_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_current_location_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], CurrentLocationComponent);



/***/ }),

/***/ 1777:
/*!*********************************************************!*\
  !*** ./src/app/pages/explore/explore-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ExplorePageRoutingModule": () => (/* binding */ ExplorePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _explore_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./explore.page */ 7362);




const routes = [
    {
        path: '',
        component: _explore_page__WEBPACK_IMPORTED_MODULE_0__.ExplorePage
    }
];
let ExplorePageRoutingModule = class ExplorePageRoutingModule {
};
ExplorePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ExplorePageRoutingModule);



/***/ }),

/***/ 8227:
/*!*************************************************!*\
  !*** ./src/app/pages/explore/explore.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ExplorePageModule": () => (/* binding */ ExplorePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _explore_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./explore-routing.module */ 1777);
/* harmony import */ var _explore_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./explore.page */ 7362);
/* harmony import */ var src_app_components_header_header_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/header/header.module */ 4546);








let ExplorePageModule = class ExplorePageModule {
};
ExplorePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _explore_routing_module__WEBPACK_IMPORTED_MODULE_0__.ExplorePageRoutingModule,
            src_app_components_header_header_module__WEBPACK_IMPORTED_MODULE_2__.HeaderModule
        ],
        declarations: [_explore_page__WEBPACK_IMPORTED_MODULE_1__.ExplorePage]
    })
], ExplorePageModule);



/***/ }),

/***/ 7362:
/*!***********************************************!*\
  !*** ./src/app/pages/explore/explore.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ExplorePage": () => (/* binding */ ExplorePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_explore_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./explore.page.html */ 9847);
/* harmony import */ var _explore_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./explore.page.scss */ 8995);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var src_app_components_current_location_current_location_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/current-location/current-location.component */ 8087);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../base-page/base-page */ 4282);
/* harmony import */ var _destination_destination_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../destination/destination.page */ 300);







let ExplorePage = class ExplorePage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__.BasePage {
    constructor(injector) {
        super(injector);
        this.slidesConfig = {
            slidesPerView: 1.6,
            spaceBetween: 20,
            initialSlide: 1,
            zoom: false,
            autoplayDisableOnInteraction: false,
            loop: true,
            speed: 500,
            breakpointsInverse: false,
            // autoplay: {
            //   delay: 2000,
            // },
            breakpoints: {
                992: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                    loop: false,
                },
            },
        };
        this.slides = [];
        this.news = [];
        this.races = [];
        this.categories = [];
        this.destinations = [];
        this.adventures = [
            {
                image: 'assets/races/race2.png',
            },
            {
                image: 'assets/races/race3.jpeg',
            },
            {
                image: 'assets/races/race2.png',
            },
        ];
        this.list = ['1', '2', '3'];
    }
    ngOnInit() {
        this.initialize();
    }
    initialize() {
        this.categories = this.dataService.getHomeCategories();
        this.destinations = this.dataService.getDestinations();
    }
    raceEventDetail(id) {
        this.nav.push('pages/tabbar/races');
    }
    openCurrentLocModal() {
        this.modals.present(src_app_components_current_location_current_location_component__WEBPACK_IMPORTED_MODULE_2__.CurrentLocationComponent);
    }
    bookInit() {
        this.nav.push('pages/book-initial');
    }
    segmentChanged($event) {
        console.log($event);
    }
    openDestinations() {
        this.modals.present(_destination_destination_page__WEBPACK_IMPORTED_MODULE_4__.DestinationPage);
    }
    categorySelected(item) {
        this.categories.filter((x) => x.selected)[0].selected = false;
        item.selected = true;
        //console.log(item);
    }
    goToSteps() {
        this.nav.push('pages/book-initial');
    }
};
ExplorePage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injector }
];
ExplorePage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-explore',
        template: _raw_loader_explore_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_explore_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ExplorePage);



/***/ }),

/***/ 5665:
/*!*****************************************************************************!*\
  !*** ./src/app/components/current-location/current-location.component.scss ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".title {\n  font-size: 2rem;\n}\n\n.peragraph {\n  color: #918989 !important;\n  margin: 2% 0 0 0;\n}\n\n.map {\n  margin: 10% 0 0 0;\n  width: 100%;\n  height: 45vh;\n  background-color: #1a1717;\n}\n\n.footer {\n  background-color: #222224;\n  width: 100%;\n  height: 30vh;\n  border-radius: 30px 30px 0 0;\n  margin: -10% 0 0 0;\n}\n\n.footer-title {\n  font-size: 1.5rem;\n  position: relative;\n  top: 20%;\n  left: 5%;\n}\n\n.footer-peragraph {\n  font-size: 14px;\n  margin: 12% 0 0 5%;\n  color: #918989 !important;\n}\n\n.footer-btn {\n  border-radius: 30px;\n  width: 90%;\n  height: 45px;\n  margin: 10% 5% 0 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImN1cnJlbnQtbG9jYXRpb24uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFBO0FBQ0o7O0FBQ0E7RUFDSSx5QkFBQTtFQUNBLGdCQUFBO0FBRUo7O0FBQUE7RUFDSSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7QUFHSjs7QUFEQTtFQUNJLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSw0QkFBQTtFQUNBLGtCQUFBO0FBSUo7O0FBRkE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7QUFLSjs7QUFIQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0FBTUo7O0FBSEE7RUFDSSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFNSiIsImZpbGUiOiJjdXJyZW50LWxvY2F0aW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRpdGxle1xyXG4gICAgZm9udC1zaXplOiAycmVtO1xyXG59XHJcbi5wZXJhZ3JhcGh7XHJcbiAgICBjb2xvcjogcmdiKDE0NSwgMTM3LCAxMzcpICFpbXBvcnRhbnQ7XHJcbiAgICBtYXJnaW46IDIlIDAgMCAwO1xyXG59XHJcbi5tYXB7XHJcbiAgICBtYXJnaW46IDEwJSAwIDAgMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA0NXZoO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI2LCAyMywgMjMpO1xyXG59XHJcbi5mb290ZXJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMzQsIDM0LCAzNik7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMzB2aDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDMwcHggMzBweCAwIDA7XHJcbiAgICBtYXJnaW46IC0xMCUgMCAwIDA7XHJcbn1cclxuLmZvb3Rlci10aXRsZXtcclxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAyMCU7XHJcbiAgICBsZWZ0OiA1JTtcclxufVxyXG4uZm9vdGVyLXBlcmFncmFwaHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIG1hcmdpbjogMTIlIDAgMCA1JTtcclxuICAgIGNvbG9yOiByZ2IoMTQ1LCAxMzcsIDEzNykgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmZvb3Rlci1idG57XHJcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGhlaWdodDogNDVweDtcclxuICAgIG1hcmdpbjogMTAlIDUlIDAgNSU7XHJcbn0iXX0= */");

/***/ }),

/***/ 8995:
/*!*************************************************!*\
  !*** ./src/app/pages/explore/explore.page.scss ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".segment-font-color {\n  color: var(--ion-color-tertiary);\n}\n\n.header-padding-top {\n  padding-top: 50px;\n}\n\n.list-image {\n  border-radius: 20px;\n  flex-shrink: 0;\n}\n\n.tab-item {\n  --background: #fff;\n  padding: 5px 20px;\n  text-align: center;\n  text-transform: uppercase;\n  font-size: 12px;\n}\n\n.destination-img {\n  height: 250px;\n  object-fit: cover;\n  --border-radius: 10px;\n}\n\nion-img::part(image) {\n  border-radius: 15px;\n}\n\nion-img {\n  width: 100%;\n  height: 250px;\n  margin: 0 auto;\n}\n\n.scrolling-wrapper {\n  overflow-x: scroll;\n  overflow-y: hidden;\n  white-space: nowrap;\n}\n\n.scrolling-wrapper .card {\n  display: inline-block;\n  width: 70%;\n}\n\n.scrolling-wrapper .category {\n  display: inline-block;\n  width: 25%;\n  margin-left: 5px;\n}\n\n.scrolling-wrapper .item {\n  display: inline-block;\n  width: 40%;\n}\n\n/* Hide scrollbar for Chrome, Safari and Opera */\n\n.scrolling-wrapper::-webkit-scrollbar {\n  display: none;\n}\n\n/* Hide scrollbar for IE, Edge and Firefox */\n\n.scrolling-wrapper {\n  -ms-overflow-style: none;\n  /* IE and Edge */\n  scrollbar-width: none;\n  /* Firefox */\n}\n\n.sildeStyle {\n  width: 70%;\n  height: 250px;\n  border-radius: 15px;\n  object-fit: cover;\n  object-position: center;\n}\n\n.sildeStyle2 {\n  width: 40%;\n  height: 280px;\n  border-radius: 15px;\n  object-fit: cover;\n  object-position: center;\n}\n\n.heart-box-outer {\n  position: relative;\n}\n\n.heart-box-outer .heart-box {\n  position: absolute;\n  top: 10px;\n  right: 90px;\n  color: var(--ion-color-tertiary);\n  font-size: 30px;\n}\n\n.heart-box-outer .image-Title-Text {\n  position: absolute;\n  left: 90px;\n  color: var(--ion-color-tertiary);\n  bottom: 35px;\n}\n\n.heart-box-outer .image-Location-Text {\n  position: absolute;\n  left: 85px;\n  color: var(--ion-color-tertiary);\n  bottom: 0;\n}\n\n.heart-box-outer .heart-box2 {\n  position: absolute;\n  top: 10px;\n  right: 160px;\n  color: var(--ion-color-tertiary);\n  font-size: 30px;\n}\n\n.heart-box-outer .image-Title-Text2 {\n  position: absolute;\n  left: 160px;\n  color: var(--ion-color-tertiary);\n  bottom: 35px;\n}\n\n.heart-box-outer .image-Location-Text2 {\n  position: absolute;\n  left: 153px;\n  color: var(--ion-color-tertiary);\n  bottom: 0;\n}\n\n.heart-box-outer .icon-size {\n  font-size: 17px;\n  margin-top: 20px;\n}\n\n.divStyle {\n  width: 70%;\n  height: 250px;\n  position: relative;\n  border-radius: 15px;\n  margin-bottom: 20px;\n  position: relative;\n}\n\n.divStyle .firstimage {\n  width: 100%;\n  height: 100%;\n  border-radius: 10px;\n  object-fit: cover;\n  object-position: center;\n}\n\n.divStyle .countryTitle {\n  position: absolute;\n  bottom: 25px;\n  left: 15px;\n}\n\n.divStyle .icon {\n  position: absolute;\n  bottom: 230px;\n  right: 15px;\n  font-size: 22px;\n}\n\n.divStyle .rowStyle {\n  position: absolute;\n  bottom: 0;\n  left: 15px;\n}\n\n.divStyle2 {\n  width: 45%;\n  height: 300px;\n  position: relative;\n  border-radius: 15px;\n  margin-bottom: 20px;\n  position: relative;\n}\n\n.divStyle2 .firstimage {\n  width: 100%;\n  height: 100%;\n  border-radius: 10px;\n  object-fit: cover;\n  object-position: center;\n}\n\n.divStyle2 .countryTitle {\n  position: absolute;\n  bottom: 25px;\n  left: 15px;\n}\n\n.divStyle2 .icon {\n  position: absolute;\n  bottom: 280px;\n  right: 15px;\n  font-size: 22px;\n}\n\n.divStyle2 .rowStyle {\n  position: absolute;\n  bottom: 0;\n  left: 15px;\n}\n\n.vendor-slide {\n  width: 50% !important;\n  height: 280px !important;\n  border-radius: 10px;\n}\n\nion-slides .slidestyle {\n  background-color: #dedede !important;\n  width: 100% !important;\n  margin: 0 auto;\n  border-radius: 10px;\n}\n\nion-slides ion-slide img {\n  border-radius: 10px;\n  display: block;\n  width: 100%;\n  height: 200px;\n  object-fit: cover;\n  object-position: center;\n  margin: 0 auto;\n}\n\nion-slides {\n  border-radius: 15px;\n}\n\nion-slides ion-slide img {\n  border-radius: 15px;\n  display: block;\n  width: 100%;\n  height: 220px;\n  object-fit: cover;\n  object-position: center;\n}\n\n.outerClass {\n  position: relative;\n  margin-left: 15px;\n  border-radius: 20px;\n}\n\n.outerClass .textStyle {\n  position: absolute;\n  bottom: 20px !important;\n  font-weight: bold;\n  font-size: 2.1vh;\n  left: 15px;\n}\n\n.outerClass .icon2 {\n  position: absolute;\n  top: 10px;\n  right: 15px;\n  font-size: 22px;\n}\n\n.outerClass .rowStyle2 {\n  position: absolute;\n  bottom: 0;\n  left: 12px;\n  display: flex;\n  align-items: center;\n}\n\n.outerClass .icon-size2 {\n  font-size: 15px;\n}\n\n.unSelectedCategory {\n  color: black;\n}\n\n.selectedCategory {\n  color: var(--ion-color-tertiary);\n}\n\n.category {\n  font-size: 2vh;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImV4cGxvcmUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQVVBO0VBQ0UsZ0NBQUE7QUFURjs7QUFZQTtFQUNFLGlCQUFBO0FBVEY7O0FBV0E7RUFDRSxtQkFBQTtFQUdBLGNBQUE7QUFWRjs7QUFjQTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtBQVhGOztBQWNBO0VBQ0UsYUFBQTtFQUNBLGlCQUFBO0VBQ0EscUJBQUE7QUFYRjs7QUFjQTtFQUNFLG1CQUFBO0FBWEY7O0FBY0E7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUFYRjs7QUFjQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQVhGOztBQWNFO0VBQ0UscUJBQUE7RUFDQSxVQUFBO0FBWko7O0FBZ0JFO0VBQ0UscUJBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUFkSjs7QUFpQkU7RUFDRSxxQkFBQTtFQUNBLFVBQUE7QUFmSjs7QUFvQkEsZ0RBQUE7O0FBQ0E7RUFDRSxhQUFBO0FBakJGOztBQW9CQSw0Q0FBQTs7QUFDQTtFQUNFLHdCQUFBO0VBQTBCLGdCQUFBO0VBQzFCLHFCQUFBO0VBQXVCLFlBQUE7QUFmekI7O0FBaUJBO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsdUJBQUE7QUFkRjs7QUFnQkE7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSx1QkFBQTtBQWJGOztBQWVBO0VBQ0Usa0JBQUE7QUFaRjs7QUFhRTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxnQ0FBQTtFQUNBLGVBQUE7QUFYSjs7QUFhRTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLGdDQUFBO0VBQ0EsWUFBQTtBQVhKOztBQWFFO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsZ0NBQUE7RUFDQSxTQUFBO0FBWEo7O0FBYUU7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsZ0NBQUE7RUFDQSxlQUFBO0FBWEo7O0FBYUU7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxnQ0FBQTtFQUNBLFlBQUE7QUFYSjs7QUFhRTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGdDQUFBO0VBQ0EsU0FBQTtBQVhKOztBQWFFO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0FBWEo7O0FBY0E7RUFDRSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBWEY7O0FBWUU7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSx1QkFBQTtBQVZKOztBQVlFO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtBQVZKOztBQVlFO0VBQ0Usa0JBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFWSjs7QUFZRTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7QUFWSjs7QUFhQTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUFWRjs7QUFXRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0FBVEo7O0FBV0U7RUFDRSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0FBVEo7O0FBV0U7RUFDRSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQVRKOztBQVdFO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtBQVRKOztBQVlBO0VBRUUscUJBQUE7RUFFQSx3QkFBQTtFQUNBLG1CQUFBO0FBWEY7O0FBZ0JFO0VBQ0Usb0NBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQWJKOztBQWVFO0VBQ0UsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0VBQ0EsY0FBQTtBQWJKOztBQW9CQTtFQUdFLG1CQUFBO0FBbkJGOztBQXFCRTtFQUNFLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSx1QkFBQTtBQW5CSjs7QUF1QkE7RUFDRSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUFwQkY7O0FBcUJFO0VBQ0Usa0JBQUE7RUFDQSx1QkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxVQUFBO0FBbkJKOztBQXFCRTtFQUNFLGtCQUFBO0VBRUEsU0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBcEJKOztBQXNCRTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUFwQko7O0FBc0JFO0VBQ0UsZUFBQTtBQXBCSjs7QUF5QkE7RUFDRSxZQUFBO0FBdEJGOztBQXlCQTtFQUNFLGdDQUFBO0FBdEJGOztBQXlCQTtFQUNFLGNBQUE7QUF0QkYiLCJmaWxlIjoiZXhwbG9yZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBpb24tc2VhcmNoYmFyIHtcclxuLy8gICAtLWJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbi8vICAgLS1iYWNrZ3JvdW5kOiBibGFjaztcclxuLy8gICAtLWNvbG9yOiB3aGl0ZTtcclxuLy8gICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4vLyB9XHJcblxyXG4vLyBpb24tc2xpZGVzIHtcclxuLy8gICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTogd2hpdGU7XHJcbi8vIH1cclxuLnNlZ21lbnQtZm9udC1jb2xvciB7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeSk7XHJcbn1cclxuXHJcbi5oZWFkZXItcGFkZGluZy10b3Age1xyXG4gIHBhZGRpbmctdG9wOiA1MHB4O1xyXG59XHJcbi5saXN0LWltYWdlIHtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gIC8vIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgLy8gaGVpZ2h0OiAxNTBweDtcclxuICBmbGV4LXNocmluazogMDtcclxuICAvLyBtaW4td2lkdGg6IDEwMCU7XHJcbiAgLy8gbWluLWhlaWdodDogMTAwJTtcclxufVxyXG4udGFiLWl0ZW0ge1xyXG4gIC0tYmFja2dyb3VuZDogI2ZmZjtcclxuICBwYWRkaW5nOiA1cHggMjBweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBmb250LXNpemU6IDEycHg7XHJcbn1cclxuXHJcbi5kZXN0aW5hdGlvbi1pbWcge1xyXG4gIGhlaWdodDogMjUwcHg7XHJcbiAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcblxyXG5pb24taW1nOjpwYXJ0KGltYWdlKSB7XHJcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcclxufVxyXG5cclxuaW9uLWltZyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAyNTBweDtcclxuICBtYXJnaW46IDAgYXV0bztcclxufVxyXG5cclxuLnNjcm9sbGluZy13cmFwcGVyIHtcclxuICBvdmVyZmxvdy14OiBzY3JvbGw7XHJcbiAgb3ZlcmZsb3cteTogaGlkZGVuO1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcblxyXG4gIC8vIG1hcmdpbi10b3A6IC0xMHB4O1xyXG4gIC5jYXJkIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHdpZHRoOiA3MCU7XHJcbiAgICAvLyBtYXJnaW4tbGVmdDogMTBweDtcclxuICB9XHJcblxyXG4gIC5jYXRlZ29yeSB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICB9XHJcblxyXG4gIC5pdGVtIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHdpZHRoOiA0MCU7XHJcbiAgICAvLyBtYXJnaW4tbGVmdDogMTBweDtcclxuICB9XHJcbn1cclxuXHJcbi8qIEhpZGUgc2Nyb2xsYmFyIGZvciBDaHJvbWUsIFNhZmFyaSBhbmQgT3BlcmEgKi9cclxuLnNjcm9sbGluZy13cmFwcGVyOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuLyogSGlkZSBzY3JvbGxiYXIgZm9yIElFLCBFZGdlIGFuZCBGaXJlZm94ICovXHJcbi5zY3JvbGxpbmctd3JhcHBlciB7XHJcbiAgLW1zLW92ZXJmbG93LXN0eWxlOiBub25lOyAvKiBJRSBhbmQgRWRnZSAqL1xyXG4gIHNjcm9sbGJhci13aWR0aDogbm9uZTsgLyogRmlyZWZveCAqL1xyXG59XHJcbi5zaWxkZVN0eWxlIHtcclxuICB3aWR0aDogNzAlO1xyXG4gIGhlaWdodDogMjUwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICBvYmplY3QtZml0OiBjb3ZlcjtcclxuICBvYmplY3QtcG9zaXRpb246IGNlbnRlcjtcclxufVxyXG4uc2lsZGVTdHlsZTIge1xyXG4gIHdpZHRoOiA0MCU7XHJcbiAgaGVpZ2h0OiAyODBweDtcclxuICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gIG9iamVjdC1wb3NpdGlvbjogY2VudGVyO1xyXG59XHJcbi5oZWFydC1ib3gtb3V0ZXIge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAuaGVhcnQtYm94IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMTBweDtcclxuICAgIHJpZ2h0OiA5MHB4O1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeSk7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgfVxyXG4gIC5pbWFnZS1UaXRsZS1UZXh0IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDkwcHg7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXRlcnRpYXJ5KTtcclxuICAgIGJvdHRvbTogMzVweDtcclxuICB9XHJcbiAgLmltYWdlLUxvY2F0aW9uLVRleHQge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogODVweDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnkpO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gIH1cclxuICAuaGVhcnQtYm94MiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDEwcHg7XHJcbiAgICByaWdodDogMTYwcHg7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXRlcnRpYXJ5KTtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICB9XHJcbiAgLmltYWdlLVRpdGxlLVRleHQyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IDE2MHB4O1xyXG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeSk7XHJcbiAgICBib3R0b206IDM1cHg7XHJcbiAgfVxyXG4gIC5pbWFnZS1Mb2NhdGlvbi1UZXh0MiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiAxNTNweDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnkpO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gIH1cclxuICAuaWNvbi1zaXplIHtcclxuICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgfVxyXG59XHJcbi5kaXZTdHlsZSB7XHJcbiAgd2lkdGg6IDcwJTtcclxuICBoZWlnaHQ6IDI1MHB4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIC5maXJzdGltYWdlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgb2JqZWN0LXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5jb3VudHJ5VGl0bGUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAyNXB4O1xyXG4gICAgbGVmdDogMTVweDtcclxuICB9XHJcbiAgLmljb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAyMzBweDtcclxuICAgIHJpZ2h0OiAxNXB4O1xyXG4gICAgZm9udC1zaXplOiAyMnB4O1xyXG4gIH1cclxuICAucm93U3R5bGUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgbGVmdDogMTVweDtcclxuICB9XHJcbn1cclxuLmRpdlN0eWxlMiB7XHJcbiAgd2lkdGg6IDQ1JTtcclxuICBoZWlnaHQ6IDMwMHB4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIC5maXJzdGltYWdlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgb2JqZWN0LXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5jb3VudHJ5VGl0bGUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAyNXB4O1xyXG4gICAgbGVmdDogMTVweDtcclxuICB9XHJcbiAgLmljb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAyODBweDtcclxuICAgIHJpZ2h0OiAxNXB4O1xyXG4gICAgZm9udC1zaXplOiAyMnB4O1xyXG4gIH1cclxuICAucm93U3R5bGUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgbGVmdDogMTVweDtcclxuICB9XHJcbn1cclxuLnZlbmRvci1zbGlkZSB7XHJcbiAgLy8gYmFja2dyb3VuZC1jb2xvcjogI2RlZGVkZSAhaW1wb3J0YW50O1xyXG4gIHdpZHRoOiA1MCUgIWltcG9ydGFudDtcclxuICAvLyBtYXJnaW46IDAgYXV0bztcclxuICBoZWlnaHQ6IDI4MHB4ICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG5pb24tc2xpZGVzIHtcclxuICAvLyAtLWJ1bGxldC1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAvLyAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTogdmFyKC0taW9uLWNvbG9yLWdyYXktc2hhZGUpO1xyXG4gIC5zbGlkZXN0eWxlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNkZWRlZGUgIWltcG9ydGFudDtcclxuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgfVxyXG4gIGlvbi1zbGlkZSBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgICBvYmplY3QtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gIH1cclxufVxyXG4uc2xpZGVyLWltYWdlLXJhdGlvIHtcclxuICAvLyBoZWlnaHQ6IDI1MHB4ICFpbXBvcnRhbnQ7XHJcbiAgLy8gd2lkdGg6IDI1MHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuaW9uLXNsaWRlcyB7XHJcbiAgLy8gLS1idWxsZXQtYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgLy8gLS1idWxsZXQtYmFja2dyb3VuZC1hY3RpdmU6IHZhcigtLWlvbi1jb2xvci1ncmF5LXNoYWRlKTtcclxuICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG5cclxuICBpb24tc2xpZGUgaW1nIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAyMjBweDtcclxuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgb2JqZWN0LXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgfVxyXG59XHJcblxyXG4ub3V0ZXJDbGFzcyB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgLnRleHRTdHlsZSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBib3R0b206IDIwcHggIWltcG9ydGFudDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1zaXplOiAyLjF2aDtcclxuICAgIGxlZnQ6IDE1cHg7XHJcbiAgfVxyXG4gIC5pY29uMiB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAvLyBib3R0b206IDIzMHB4O1xyXG4gICAgdG9wOiAxMHB4O1xyXG4gICAgcmlnaHQ6IDE1cHg7XHJcbiAgICBmb250LXNpemU6IDIycHg7XHJcbiAgfVxyXG4gIC5yb3dTdHlsZTIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgbGVmdDogMTJweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIH1cclxuICAuaWNvbi1zaXplMiB7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAvLyBtYXJnaW4tdG9wOiAyMHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcblxyXG4udW5TZWxlY3RlZENhdGVnb3J5IHtcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5zZWxlY3RlZENhdGVnb3J5IHtcclxuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXRlcnRpYXJ5KTtcclxufVxyXG5cclxuLmNhdGVnb3J5IHtcclxuICBmb250LXNpemU6IDJ2aDtcclxufVxyXG5cclxuLy8gLnRvcC1iZyB7XHJcbi8vICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4vLyAgIGhlaWdodDogMzAwcHg7XHJcbi8vICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuLy8gICBtYXJnaW4tdG9wOiAtNTBweDtcclxuLy8gICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbi8vICAgei1pbmRleDogMTtcclxuLy8gICB0b3A6IDA7XHJcbi8vICAgbGVmdDogMDtcclxuLy8gICByaWdodDogMDtcclxuLy8gICBoZWlnaHQ6IDI1MHB4O1xyXG4vLyB9XHJcblxyXG4vLyAudGl0bGUge1xyXG4vLyAgIGZvbnQtd2VpZ2h0OiA5MDA7XHJcbi8vICAgZm9udC1zaXplOiAxLjVyZW07XHJcbi8vICAgY29sb3I6IHdoaXRlO1xyXG4vLyB9XHJcblxyXG4vLyAuc2lkZS1pY29uIHtcclxuLy8gICBmb250LXNpemU6IDQwcHg7XHJcbi8vICAgY29sb3I6IHdoaXRlO1xyXG4vLyB9XHJcblxyXG4vLyAuaW1hZ2Uge1xyXG4vLyAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbi8vICAgd2lkdGg6IFwiMTAwJVwiO1xyXG4vLyAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbi8vICAgaGVpZ2h0OiAyMjBweDtcclxuLy8gfVxyXG5cclxuLy8gLmRldGFpbHMtYnRuIHtcclxuLy8gICBwb3NpdGlvbjogYWJzb3VsdXRlO1xyXG4vLyAgIGJvdHRvbTogOHB4O1xyXG4vLyAgIGxlZnQ6IDE2cHg7XHJcbi8vIH1cclxuXHJcbi8vIC5zbGlkZXMge1xyXG4vLyAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9jYXIuanBnXCIpO1xyXG4vLyAgIGhlaWdodDogMjIwcHg7XHJcbi8vICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuLy8gICB3aWR0aDogOTMlO1xyXG4vLyAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbi8vIH1cclxuXHJcbi8vIC5jZW50ZXIge1xyXG4vLyAgIGRpc3BsYXk6IGJsb2NrO1xyXG4vLyAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4vLyAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuLy8gICB3aWR0aDogNTAlO1xyXG4vLyB9XHJcblxyXG4vLyAubGlzdC1oZWFkZXIge1xyXG4vLyAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbi8vICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbi8vICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuLy8gfVxyXG5cclxuLy8gLmxpc3Qtcm93IHtcclxuLy8gICAvLyB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4vLyAgIC8vIG92ZXJmbG93LXg6IGF1dG8gIWltcG9ydGFudDtcclxuLy8gICBmbGV4LXdyYXA6IG5vd3JhcDtcclxuLy8gICBvdmVyZmxvdy14OiBzY3JvbGwgIWltcG9ydGFudDtcclxuLy8gICBvdmVyZmxvdy15OiBoaWRkZW47XHJcbi8vIH1cclxuXHJcbi8vIC5saXN0LWltYWdlIHtcclxuLy8gICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4vLyAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbi8vICAgaGVpZ2h0OiAxNTBweDtcclxuLy8gICBmbGV4LXNocmluazogMDtcclxuLy8gICBtaW4td2lkdGg6IDEwMCU7XHJcbi8vICAgbWluLWhlaWdodDogMTAwJTtcclxuLy8gfVxyXG5cclxuLy8gLmNvbnRlbnQge1xyXG4vLyAgIHdpZHRoOiA5MCU7XHJcbi8vIH1cclxuXHJcbi8vIC5zY3JvbGxpbmctd3JhcHBlciB7XHJcbi8vICAgb3ZlcmZsb3cteDogc2Nyb2xsO1xyXG4vLyAgIG92ZXJmbG93LXk6IGhpZGRlbjtcclxuLy8gICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4vLyAgIG1hcmdpbi10b3A6IC0xMHB4O1xyXG4vLyAgIC5jYXJkIHtcclxuLy8gICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuLy8gICAgIHdpZHRoOiA4MCU7XHJcbi8vICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuLy8gICB9XHJcbi8vIH1cclxuXHJcbi8vIC5zY3JvbGxpbmctd3JhcHBlcjo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4vLyAgIGRpc3BsYXk6IG5vbmU7XHJcbi8vIH1cclxuXHJcbi8vIC5zY3JvbGxpbmctd3JhcHBlci1mbGV4Ym94IHtcclxuLy8gICBkaXNwbGF5OiBmbGV4O1xyXG4vLyAgIGZsZXgtd3JhcDogbm93cmFwO1xyXG4vLyAgIG92ZXJmbG93LXg6IGhpZGRlbjtcclxuXHJcbi8vICAgLmNhcmQge1xyXG4vLyAgICAgZmxleDogMCAwIGF1dG87XHJcbi8vICAgfVxyXG4vLyB9XHJcblxyXG4vLyAubGlzdC1idG4ge1xyXG4vLyAgIHdpZHRoOiBhdXRvO1xyXG4vLyB9XHJcblxyXG4vLyAudGFiLWl0ZW0ge1xyXG4vLyAgIC0tYmFja2dyb3VuZDogI2ZmZjtcclxuLy8gICBwYWRkaW5nOiA1cHggMjBweDtcclxuLy8gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuLy8gICBmb250LXNpemU6IDEycHg7XHJcbi8vICAgaW9uLWltZyB7XHJcbi8vICAgICB3aWR0aDogMzBweDtcclxuLy8gICAgIG1hcmdpbjogMCBhdXRvO1xyXG4vLyAgIH1cclxuLy8gfVxyXG4iXX0= */");

/***/ }),

/***/ 4030:
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/current-location/current-location.component.html ***!
  \*******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"this.modals.dismiss()\">\r\n        <ion-icon name=\"close-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n<ion-content color=\"primary\">\r\n  <ion-item color=\"primary\">\r\n    <ion-label>\r\n      <h2 class=\"title\">Hello, Jhon epam</h2>\r\n      <p class=\"peragraph\">What's your current location?</p>\r\n    </ion-label>\r\n  </ion-item>\r\n  <ion-searchbar color=\"dark\" placeholder=\"Search Products\"></ion-searchbar>\r\n  <div class=\"map\">\r\n    <div #map id=\"map\" style=\"height:100%;width:100%\"></div>\r\n  </div>\r\n  <div class=\"footer\">\r\n    <h3 class=\"footer-title\">The Ritz London</h3>\r\n    <p class=\"footer-peragraph\">St. Jame's London W2J 9BR</p>\r\n    <ion-button class=\"footer-btn\" color=\"secondary\" expand=\"block\">Save Location</ion-button>\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ 9847:
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/explore/explore.page.html ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<app-header [title]=\"'Discover'\"> </app-header>\r\n\r\n<ion-content color=\"primary\">\r\n  <!-- <ion-button color=\"primary\" expand=\"block\" (click)=\"openCurrentLocModal()\">Add Location</ion-button>\r\n  <ion-button color=\"primary\" expand=\"block\" (click)=\"bookInit()\">Book Initial</ion-button> -->\r\n  <!-- <ion-segment (ionChange)=\"segmentChanged($event)\">\r\n    <ion-segment-button value=\"trend\">\r\n      <ion-label class=\"segment-font-color\">Trend</ion-label>\r\n    </ion-segment-button>\r\n    <ion-segment-button value=\"featured\">\r\n      <ion-label class=\"segment-font-color\">featured</ion-label>\r\n    </ion-segment-button>\r\n    <ion-segment-button value=\"top-visit\">\r\n      <ion-label class=\"segment-font-color\">Top Visit</ion-label>\r\n    </ion-segment-button>\r\n  </ion-segment> -->\r\n\r\n  <ion-grid class=\"ion-padding-top\">\r\n    <div class=\"scrolling-wrapper ion-margin-top\">\r\n      <div\r\n        class=\"category\"\r\n        *ngFor=\"let item of categories\"\r\n        (click)=\"categorySelected(item)\"\r\n      >\r\n        <ion-label>\r\n          <ion-text [color]=\"item.selected == true ? 'secondary' : 'light'\">\r\n            {{item.name}}\r\n          </ion-text>\r\n        </ion-label>\r\n        <!-- <h3 class=\"category\">{{item.name}}</h3> -->\r\n      </div>\r\n    </div>\r\n\r\n    <ion-row class=\"ion-margin-top ion-padding-top\">\r\n      <!-- <ion-col size=\"8\"> -->\r\n      <ion-slides loop=\"true\" pager=\"false\" [options]=\"slidesConfig\">\r\n        <ion-slide\r\n          mode=\"ios\"\r\n          *ngFor=\"let item of destinations\"\r\n          (click)=\"goToSteps()\"\r\n        >\r\n          <div class=\"outerClass\">\r\n            <img class=\"slider-image-ratio\" [src]=\"item.image\" />\r\n            <ion-icon class=\"icon2\" name=\"heart-outline\"></ion-icon>\r\n            <h3 class=\"textStyle\">{{item.name}}</h3>\r\n\r\n            <ion-row class=\"rowStyle2\">\r\n              <ion-icon class=\"icon-size2\" name=\"location-outline\"></ion-icon>\r\n              <p style=\"font-size: 1.5vh\">{{item.location}}</p>\r\n            </ion-row>\r\n          </div>\r\n        </ion-slide>\r\n      </ion-slides>\r\n      <!-- </ion-col> -->\r\n    </ion-row>\r\n\r\n    <!-- <ion-row class=\"ion-margin-top\">\r\n      <div class=\"scrolling-wrapper\">\r\n        <div class=\"divStyle\">\r\n          <ion-col *ngFor=\"let item of adventures\">\r\n            <img class=\"firstimage\" [src]=\"item.image\" />\r\n            <h3 class=\"countryTitle\">Dubai</h3>\r\n            <ion-icon class=\"icon\" name=\"heart-outline\"></ion-icon>\r\n            <ion-row class=\"rowStyle\">\r\n              <ion-icon\r\n                style=\"margin-top: 15px\"\r\n                class=\"icon-size\"\r\n                name=\"location-outline\"\r\n              ></ion-icon>\r\n              <p>Location</p>\r\n            </ion-row>\r\n          </ion-col>\r\n        </div>\r\n      </div>\r\n    </ion-row> -->\r\n\r\n    <ion-row class=\"ion-margin-top\">\r\n      <ion-col size=\"9.8\">\r\n        <h3 style=\"padding-left: 10px\">Popular Destinations</h3>\r\n      </ion-col>\r\n      <ion-col siz=\"2\" class=\"ion-margin-top\" (click)=\"openDestinations()\">\r\n        <ion-label color=\"secondary\"> See All </ion-label>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <div class=\"scrolling-wrapper ion-margin-top\">\r\n      <div\r\n        class=\"item outerClass\"\r\n        *ngFor=\"let item of destinations\"\r\n        (click)=\"goToSteps()\"\r\n      >\r\n        <ion-img class=\"destination-img\" [src]=\"item.image\"> </ion-img>\r\n        <ion-icon class=\"icon2\" name=\"heart-outline\"></ion-icon>\r\n        <h3 class=\"textStyle\">{{item.name}}</h3>\r\n\r\n        <ion-row class=\"rowStyle2\">\r\n          <ion-icon class=\"icon-size2\" name=\"location-outline\"></ion-icon>\r\n          <p style=\"font-size: 1.5vh\">{{item.location}}</p>\r\n        </ion-row>\r\n      </div>\r\n    </div>\r\n\r\n    <!-- <ion-row>\r\n      <div class=\"scrolling-wrapper\">\r\n        <div class=\"divStyle2\">\r\n          <ion-col *ngFor=\"let item of adventures\">\r\n            <img class=\"firstimage\" [src]=\"item.image\" />\r\n            <h3 class=\"countryTitle\">Dubai</h3>\r\n            <ion-icon class=\"icon\" name=\"heart-outline\"></ion-icon>\r\n            <ion-row class=\"rowStyle\">\r\n              <ion-icon\r\n                style=\"margin-top: 15px\"\r\n                class=\"icon-size\"\r\n                name=\"location-outline\"\r\n              ></ion-icon>\r\n              <p>Location</p>\r\n            </ion-row>\r\n          </ion-col>\r\n        </div>\r\n      </div>\r\n    </ion-row> -->\r\n  </ion-grid>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_explore_explore_module_ts.js.map
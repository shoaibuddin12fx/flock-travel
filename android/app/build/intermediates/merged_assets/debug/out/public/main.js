(self["webpackChunkVeenme"] = self["webpackChunkVeenme"] || []).push([["main"],{

/***/ 8255:
/*!*******************************************************!*\
  !*** ./$_lazy_route_resources/ lazy namespace object ***!
  \*******************************************************/
/***/ ((module) => {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(() => {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = () => ([]);
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 8255;
module.exports = webpackEmptyAsyncContext;

/***/ }),

/***/ 158:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 9895);



const routes = [
    {
        path: '',
        redirectTo: 'pages',
        pathMatch: 'full',
    },
    {
        path: 'pages',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_pages_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./pages/pages.module */ 8950)).then((m) => m.PagesModule),
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule)({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__.PreloadAllModules }),
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule],
    })
], AppRoutingModule);



/***/ }),

/***/ 5041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./app.component.html */ 1106);
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss */ 3069);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _services_basic_nav_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/basic/nav.service */ 6350);





let AppComponent = class AppComponent {
    constructor(nav) {
        this.nav = nav;
    }
    goTo(page) {
        this.nav.navigateTo(page);
    }
};
AppComponent.ctorParameters = () => [
    { type: _services_basic_nav_service__WEBPACK_IMPORTED_MODULE_2__.NavService }
];
AppComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], AppComponent);



/***/ }),

/***/ 6747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/platform-browser */ 9075);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.component */ 5041);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app-routing.module */ 158);
/* harmony import */ var _services_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/utility.service */ 7278);
/* harmony import */ var _pages_login_login_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/login/login.module */ 1053);
/* harmony import */ var _pages_signup_signup_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/signup/signup.module */ 7110);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _awesome_cordova_plugins_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @awesome-cordova-plugins/ionic-webview/ngx */ 7674);
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ 3760);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var _services_interceptor_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/interceptor.service */ 155);
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ 7152);
/* harmony import */ var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/launch-navigator/ngx */ 6297);
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/google-maps */ 7385);


















let AppModule = class AppModule {
};
AppModule = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.NgModule)({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_0__.AppComponent],
        entryComponents: [],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_13__.BrowserModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_14__.IonicModule.forRoot(),
            _angular_common_http__WEBPACK_IMPORTED_MODULE_15__.HttpClientModule,
            _app_routing_module__WEBPACK_IMPORTED_MODULE_1__.AppRoutingModule,
            _pages_login_login_module__WEBPACK_IMPORTED_MODULE_3__.LoginPageModule,
            _pages_signup_signup_module__WEBPACK_IMPORTED_MODULE_4__.SignupPageModule
        ],
        providers: [
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_16__.RouteReuseStrategy, useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_14__.IonicRouteStrategy },
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_15__.HTTP_INTERCEPTORS, useClass: _services_interceptor_service__WEBPACK_IMPORTED_MODULE_7__.InterceptorService, multi: true },
            _angular_forms__WEBPACK_IMPORTED_MODULE_17__.FormBuilder,
            _services_utility_service__WEBPACK_IMPORTED_MODULE_2__.UtilityService,
            _awesome_cordova_plugins_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_5__.WebView,
            _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__.InAppBrowser,
            _services_utility_service__WEBPACK_IMPORTED_MODULE_2__.UtilityService,
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__.Geolocation,
            _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_9__.LaunchNavigator,
            _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_10__.GoogleMaps,
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_0__.AppComponent],
    })
], AppModule);



/***/ }),

/***/ 4282:
/*!**********************************************!*\
  !*** ./src/app/pages/base-page/base-page.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BasePage": () => (/* binding */ BasePage)
/* harmony export */ });
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/platform-browser */ 9075);
/* harmony import */ var src_app_services_basic_modal_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/services/basic/modal.service */ 4307);
/* harmony import */ var src_app_services_basic_nav_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/basic/nav.service */ 6350);
/* harmony import */ var src_app_services_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/utility.service */ 7278);
/* harmony import */ var src_app_services_basic_events_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/basic/events.service */ 3511);
/* harmony import */ var src_app_services_network_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/network.service */ 2982);
/* harmony import */ var src_app_services_basic_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/basic/storage.service */ 5911);
/* harmony import */ var src_app_services_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/user.service */ 3071);
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/data.service */ 2468);











class BasePage {
    constructor(injector) {
        this.platform = injector.get(_ionic_angular__WEBPACK_IMPORTED_MODULE_8__.Platform);
        this.formBuilder = injector.get(_angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormBuilder);
        this.menuCtrl = injector.get(_ionic_angular__WEBPACK_IMPORTED_MODULE_8__.MenuController);
        this.domSanitizer = injector.get(_angular_platform_browser__WEBPACK_IMPORTED_MODULE_10__.DomSanitizer);
        this.nav = injector.get(src_app_services_basic_nav_service__WEBPACK_IMPORTED_MODULE_1__.NavService);
        this.modals = injector.get(src_app_services_basic_modal_service__WEBPACK_IMPORTED_MODULE_0__.ModalService);
        this.utility = injector.get(src_app_services_utility_service__WEBPACK_IMPORTED_MODULE_2__.UtilityService);
        this.events = injector.get(src_app_services_basic_events_service__WEBPACK_IMPORTED_MODULE_3__.EventsService);
        this.network = injector.get(src_app_services_network_service__WEBPACK_IMPORTED_MODULE_4__.NetworkService);
        this.users = injector.get(src_app_services_user_service__WEBPACK_IMPORTED_MODULE_6__.UserService);
        this.storage = injector.get(src_app_services_basic_storage_service__WEBPACK_IMPORTED_MODULE_5__.StorageService);
        this.dataService = injector.get(src_app_services_data_service__WEBPACK_IMPORTED_MODULE_7__.DataService);
    }
}


/***/ }),

/***/ 3403:
/*!*****************************************************!*\
  !*** ./src/app/pages/login/login-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageRoutingModule": () => (/* binding */ LoginPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.page */ 3058);




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_0__.LoginPage
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ 1053:
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageModule": () => (/* binding */ LoginPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-routing.module */ 3403);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page */ 3058);







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _login_routing_module__WEBPACK_IMPORTED_MODULE_0__.LoginPageRoutingModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_1__.LoginPage]
    })
], LoginPageModule);



/***/ }),

/***/ 3058:
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPage": () => (/* binding */ LoginPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./login.page.html */ 1021);
/* harmony import */ var _login_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page.scss */ 8781);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 4282);






let LoginPage = class LoginPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.loading = false;
    }
    ngOnInit() {
        this.setupForm();
    }
    setupForm() {
        const re = /\S+@\S+\.\S+/;
        this.aForm = this.formBuilder.group({
            email: [
                'john_smith22@gmail.com',
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.email]),
            ],
            password: [
                '123456',
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.minLength(6),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.maxLength(30),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required,
                ]),
            ],
        });
    }
    openSignup() {
        this.nav.navigateTo('pages/signup');
    }
    login() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            // this.nav.push('pages/tabbar');
            if (!this.aForm.valid) {
                this.utility.presentFailureToast('Pleae fill all fields properly');
                return;
            }
            const formdata = this.aForm.value;
            this.loading = true;
            const user = yield this.users.login(formdata);
            if (user) {
                this.nav.push('pages/tabbar');
            }
            else {
            }
            this.loading = false;
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injector }
];
LoginPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], LoginPage);



/***/ }),

/***/ 392:
/*!*******************************************************!*\
  !*** ./src/app/pages/signup/signup-routing.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SignupPageRoutingModule": () => (/* binding */ SignupPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./signup.page */ 4374);




const routes = [
    {
        path: '',
        component: _signup_page__WEBPACK_IMPORTED_MODULE_0__.SignupPage
    }
];
let SignupPageRoutingModule = class SignupPageRoutingModule {
};
SignupPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SignupPageRoutingModule);



/***/ }),

/***/ 7110:
/*!***********************************************!*\
  !*** ./src/app/pages/signup/signup.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SignupPageModule": () => (/* binding */ SignupPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _signup_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./signup-routing.module */ 392);
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./signup.page */ 4374);







let SignupPageModule = class SignupPageModule {
};
SignupPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _signup_routing_module__WEBPACK_IMPORTED_MODULE_0__.SignupPageRoutingModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule
        ],
        declarations: [_signup_page__WEBPACK_IMPORTED_MODULE_1__.SignupPage]
    })
], SignupPageModule);



/***/ }),

/***/ 4374:
/*!*********************************************!*\
  !*** ./src/app/pages/signup/signup.page.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SignupPage": () => (/* binding */ SignupPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_signup_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./signup.page.html */ 1979);
/* harmony import */ var _signup_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./signup.page.scss */ 9233);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var src_app_services_basic_strings_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/basic/strings.service */ 5030);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../base-page/base-page */ 4282);







let SignupPage = class SignupPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__.BasePage {
    constructor(injector, strings) {
        super(injector);
        this.strings = strings;
        this.signupObj = {
            fname: '',
            lname: '',
            email: '',
            phone: '',
            password: '',
            dob: '',
            gender: 'm',
        };
        this.loading = false;
        this.setupForm();
    }
    setupForm() {
        const re = /\S+@\S+\.\S+/;
        this.aForm = this.formBuilder.group({
            fname: [
                '',
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.minLength(3),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.maxLength(30),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.pattern('[a-zA-Z ]*'),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required,
                ]),
            ],
            lname: [
                '',
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.minLength(3),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.maxLength(30),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.pattern('[a-zA-Z ]*'),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required,
                ]),
            ],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.email])],
            phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required])],
            password: [
                '',
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.minLength(6),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.maxLength(30),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required,
                ]),
            ],
            dob: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required])],
            gender: [
                this.signupObj.gender,
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.compose([_angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required]),
            ],
        });
    }
    ngOnInit() { }
    singUp() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.nav.navigateTo('home');
            if (this.aForm.invalid) {
                this.utility.presentFailureToast('Pleae fill all fields properly');
                return;
            }
            const formdata = this.aForm.value;
            console.log(formdata);
            formdata['phone'] = '+1' + this.strings.getOnlyDigits(formdata['phone']);
            console.log(formdata);
            this.loading = true;
            // const { data } = await this.network.register(formdata);
            // console.log(data);
            // if (data) {
            //   const res = await this.user.setToken(data.token);
            //   if (res) {
            //     this.modals.dismiss({ data: res });
            //   }
            //   // await Browser.open({ url: `https://dev-veenme.thesupportonline.net/testtoken/${data.token}` });
            // }
            this.loading = false;
            this.utility.presentSuccessToast('Success');
            this.nav.navigateTo('home');
        });
    }
    onTelephoneChange(ev) {
        if (ev.inputType !== 'deleteContentBackward') {
            const utel = this.utility.onkeyupFormatPhoneNumberRuntime(ev.target.value, false);
            console.log(utel);
            ev.target.value = utel;
            this.aForm.controls['phone'].patchValue(utel);
            // ev.target.value = utel;
        }
    }
    openLogin() {
        this.nav.pop();
    }
};
SignupPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Injector },
    { type: src_app_services_basic_strings_service__WEBPACK_IMPORTED_MODULE_2__.StringsService }
];
SignupPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-signup',
        template: _raw_loader_signup_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_signup_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SignupPage);



/***/ }),

/***/ 5830:
/*!*****************************************!*\
  !*** ./src/app/services/api.service.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ApiService": () => (/* binding */ ApiService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);



let ApiService = class ApiService {
    constructor(http) {
        this.http = http;
        this.url = 'https://veenmeapi.thesupportonline.net/index.php/v1';
    }
    get(endpoint, params, reqOpts) {
        return this.http.get(this.url + '/' + endpoint, reqOpts);
    }
    post(endpoint, body, reqOpts) {
        return this.http.post(this.url + '/' + endpoint, body, reqOpts);
    }
    put(endpoint, body, reqOpts) {
        return this.http.put(this.url + '/' + endpoint, body, reqOpts);
    }
    delete(endpoint, reqOpts) {
        return this.http.delete(this.url + '/' + endpoint, reqOpts);
    }
    patch(endpoint, body, reqOpts) {
        return this.http.patch(this.url + '/' + endpoint, body, reqOpts);
    }
};
ApiService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpClient }
];
ApiService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], ApiService);



/***/ }),

/***/ 7661:
/*!**************************************************!*\
  !*** ./src/app/services/basic/alerts.service.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AlertsService": () => (/* binding */ AlertsService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _strings_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./strings.service */ 5030);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 476);





let AlertsService = class AlertsService {
    constructor(alertController, toastCtrl, strings) {
        this.alertController = alertController;
        this.toastCtrl = toastCtrl;
        this.strings = strings;
    }
    showAlert(msg) {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: 'Alert',
                message: msg,
                buttons: [
                    {
                        text: 'OK',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            resolve(true);
                        }
                    }
                ]
            });
            yield alert.present();
        }));
    }
    presentSuccessToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: this.strings.capitalizeEachFirst(msg),
                duration: 5000,
                position: 'top',
                cssClass: 'successToast',
                buttons: [
                    {
                        side: 'end',
                        icon: 'close-outline',
                        role: 'cancel',
                        cssClass: 'icon-class'
                    }
                ]
            });
            toast.present();
        });
    }
    presentFailureToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: this.strings.capitalizeEachFirst((msg) ? msg : 'ERROR'),
                duration: 5000,
                position: 'top',
                cssClass: 'failureToast',
                buttons: [
                    {
                        side: 'end',
                        icon: 'close-outline',
                        role: 'cancel',
                        cssClass: 'icon-class'
                    }
                ]
            });
            toast.present();
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: msg,
                duration: 5000,
                position: 'bottom'
            });
            toast.present();
        });
    }
    presentConfirm(okText = 'OK', cancelText = 'Cancel', title = 'Are You Sure?', message = '') {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: title,
                message,
                buttons: [
                    {
                        text: cancelText,
                        role: 'cancel',
                        handler: () => {
                            resolve(false);
                        }
                    },
                    {
                        text: okText,
                        handler: () => {
                            resolve(true);
                        }
                    }
                ]
            });
            alert.present();
        }));
    }
    presentRadioSelections(title, message, inputs, okText = 'OK', cancelText = 'Cancel') {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: title,
                message,
                inputs,
                buttons: [
                    {
                        text: cancelText,
                        role: 'cancel',
                        handler: () => {
                            resolve(false);
                        }
                    },
                    {
                        text: okText,
                        handler: (data) => {
                            resolve(data);
                        }
                    }
                ]
            });
            alert.present();
        }));
    }
};
AlertsService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.AlertController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.ToastController },
    { type: _strings_service__WEBPACK_IMPORTED_MODULE_0__.StringsService }
];
AlertsService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root'
    })
], AlertsService);



/***/ }),

/***/ 3511:
/*!**************************************************!*\
  !*** ./src/app/services/basic/events.service.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EventsService": () => (/* binding */ EventsService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);


let EventsService = class EventsService {
    constructor() {
        this.latestEvent = 'randomLast';
        this.historicalEvent = 'randomHistory';
        // pubsubSvc.registerEventWithHistory(this.historicalEvent, 6);
        // pubsubSvc.registerEventWithLastValue(this.latestEvent, undefined);
    }
    publish(key, data = {}) {
        // this.pubsubSvc.publishEvent(key, data);
    }
    subscribe(key, handler) {
        // this.pubsubSvc.subscribe(key, data =>  handler(data) );
        // this.subscriptions[key] = 
    }
    unsubscribe(key) {
        // this.pubsubSvc.subscribe(key)
        // this.subscriptions[key].unsubscribe();
    }
};
EventsService.ctorParameters = () => [];
EventsService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)({
        providedIn: 'root'
    })
], EventsService);



/***/ }),

/***/ 2537:
/*!***************************************************!*\
  !*** ./src/app/services/basic/loading.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoadingService": () => (/* binding */ LoadingService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);



let LoadingService = class LoadingService {
    constructor(loadingController) {
        this.loadingController = loadingController;
    }
    show() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            this.showLoader();
        });
    }
    hide() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            this.hideLoader();
        });
    }
    showLoader(message = 'Please wait...') {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            if (this.loading) {
                this.loading.dismiss();
            }
            this.loading = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: message,
            });
            yield this.loading.present();
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            if (this.loading) {
                this.loading.dismiss();
            }
        });
    }
};
LoadingService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__.LoadingController }
];
LoadingService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({ providedIn: 'root' })
], LoadingService);



/***/ }),

/***/ 4307:
/*!*************************************************!*\
  !*** ./src/app/services/basic/modal.service.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ModalService": () => (/* binding */ ModalService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ 476);



let ModalService = class ModalService {
    constructor(modal) {
        this.modal = modal;
    }
    present(component, data = {}, cssClass = '') {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modal.create({
                component,
                cssClass,
                componentProps: data
            });
            modal.onDidDismiss().then(res => {
                resolve(res);
            });
            yield modal.present();
        }));
    }
    dismiss(data = {}) {
        return new Promise(resolve => {
            data.dismiss = true;
            this.modal.dismiss(data).then(v => resolve(true));
        });
    }
};
ModalService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__.ModalController }
];
ModalService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], ModalService);



/***/ }),

/***/ 6350:
/*!***********************************************!*\
  !*** ./src/app/services/basic/nav.service.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NavService": () => (/* binding */ NavService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 9895);




let NavService = class NavService {
    //
    constructor(location, router, activatedRoute) {
        this.location = location;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.router.onSameUrlNavigation = 'reload';
    }
    setRoot(page, param = {}) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            // await this.nativePageTransitions.fade(null);
            const extras = {
                queryParams: param,
            };
            this.navigateTo(page, extras);
        });
    }
    push(page, param = {}) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            // await this.nativePageTransitions.slide(this.options);
            const extras = {
                queryParams: param,
            };
            this.navigateTo(page, extras);
        });
    }
    pop() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
                // await this.nativePageTransitions.slide(this.options);
                // await this.nativePageTransitions.fade(null);
                this.location.back();
                resolve();
            }));
        });
    }
    navigateTo(link, data) {
        console.log(link);
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([link], data);
    }
    navigateToChild(link, data) {
        data.relativeTo = this.activatedRoute;
        this.router.navigate([link], data);
    }
    getParams() {
        return this.activatedRoute.snapshot.params;
    }
    getQueryParams() {
        return this.activatedRoute.snapshot.queryParams;
    }
};
NavService.ctorParameters = () => [
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_1__.Location },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__.ActivatedRoute }
];
NavService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root',
    })
], NavService);



/***/ }),

/***/ 5911:
/*!***************************************************!*\
  !*** ./src/app/services/basic/storage.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StorageService": () => (/* binding */ StorageService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);


let StorageService = class StorageService {
    constructor() { }
    set(key, data) {
        return new Promise((resolve) => {
            localStorage.setItem(key, data);
            resolve(true);
        });
    }
    get(key) {
        return new Promise((resolve) => {
            const data = localStorage.getItem(key);
            resolve(data);
        });
    }
    remove(key) {
        return new Promise((resolve) => {
            const data = localStorage.removeItem(key);
            resolve(data);
        });
    }
};
StorageService.ctorParameters = () => [];
StorageService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)({
        providedIn: 'root',
    })
], StorageService);



/***/ }),

/***/ 5030:
/*!***************************************************!*\
  !*** ./src/app/services/basic/strings.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StringsService": () => (/* binding */ StringsService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);


let StringsService = class StringsService {
    constructor() { }
    getOnlyDigits(phoneNumber) {
        var numberString = phoneNumber;
        var numberInDigits = numberString.replace(/[^\d]/g, '');
        var numberVal = parseInt(numberInDigits);
        console.log(numberVal);
        return numberVal.toString();
    }
    isPhoneNumberValid(number) {
        var _validPhoneNumber = this.getOnlyDigits(number);
        // remove trailing zeros
        let s = _validPhoneNumber.toString();
        return _validPhoneNumber.toString().length < 10 ? false : true;
    }
    capitalizeEachFirst(str) {
        if (!str)
            return '';
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] =
                splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    }
    checkIfMatchingPasswords(passwordKey, passwordConfirmationKey) {
        return (group) => {
            let passwordInput = group.controls[passwordKey], passwordConfirmationInput = group.controls[passwordConfirmationKey];
            if (passwordInput.value !== passwordConfirmationInput.value) {
                return passwordConfirmationInput.setErrors({ notEquivalent: true });
            }
            else {
                return passwordConfirmationInput.setErrors(null);
            }
        };
    }
    parseAddressFromProfile(__profile) {
        return `${__profile['apartment'] || ''} ${__profile['street_address'] || ''} ${__profile['city'] || ''} ${__profile['state'] || ''} ${__profile['zip_code'] || ''}`;
    }
    parseName(input) {
        const capitalize = (s) => {
            if (typeof s !== 'string')
                return '';
            s = s.toLowerCase();
            return s.charAt(0).toUpperCase() + s.slice(1);
        };
        var fullName = input || '';
        var result = {};
        if (fullName.length > 0) {
            var nameTokens = fullName.match(/\w*/g) || [];
            nameTokens = nameTokens.filter((n) => n);
            if (nameTokens.length > 3) {
                result['firstName'] = nameTokens.slice(0, 2).join(' ');
                result['firstName'] = capitalize(result['firstName']);
            }
            else {
                result['firstName'] = nameTokens.slice(0, 1).join(' ');
                result['firstName'] = capitalize(result['firstName']);
            }
            if (nameTokens.length > 2) {
                result['middleName'] = nameTokens.slice(-2, -1).join(' ');
                result['lastName'] = nameTokens.slice(-1).join(' ');
                result['middleName'] = capitalize(result['middleName']);
                result['lastName'] = capitalize(result['lastName']);
            }
            else {
                if (nameTokens.length == 1) {
                    result['lastName'] = '';
                    result['middleName'] = '';
                }
                else {
                    result['lastName'] = nameTokens.slice(-1).join(' ');
                    result['lastName'] = capitalize(result['lastName']);
                    result['middleName'] = '';
                }
            }
        }
        var display_name = result['lastName'] +
            (result['lastName'] ? ' ' : '') +
            result['firstName'];
        return display_name;
    }
    isLastNameExist(input) {
        var fullname = this.parseName(input);
        return !(fullname['lastName'] == '');
    }
    formatPhoneNumberRuntime(phoneNumber, last = false) {
        if (phoneNumber == null || phoneNumber == '') {
            return phoneNumber;
        }
        phoneNumber = this.getOnlyDigits(phoneNumber);
        // phoneNumber = phoneNumber.substring(phoneNumber.length - 1,-11);//keep only 10 digit Number
        // phoneNumber = phoneNumber.substring(phoneNumber.length - 10, -11);//keep only 10 digit Number
        phoneNumber = last
            ? phoneNumber.substring(phoneNumber.length - 10, phoneNumber.length)
            : phoneNumber.substring(0, 10);
        const cleaned = ('' + phoneNumber).replace(/\D/g, '');
        function numDigits(x) {
            return (Math.log(x) * Math.LOG10E + 1) | 0;
        }
        // only keep number and +
        const p1 = cleaned.match(/\d+/g);
        if (p1 == null) {
            return cleaned;
        }
        const p2 = phoneNumber.match(/\d+/g).map(Number);
        const len = numDigits(p2);
        // document.write(len + " " );
        switch (len) {
            case 1:
            case 2:
                return '(' + phoneNumber;
            case 3:
                return '(' + phoneNumber + ')';
            case 4:
            case 5:
            case 6:
                var f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
                var f2 = phoneNumber.toString().substring(len, 3);
                return f1 + ' ' + f2;
            default:
                f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
                f2 = phoneNumber.toString().substring(3, 6);
                var f3 = phoneNumber.toString().substring(6, 10);
                console.log(phoneNumber, f3);
                return f1 + ' ' + f2 + '-' + f3;
        }
        // return "len";
    }
};
StringsService.ctorParameters = () => [];
StringsService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)({
        providedIn: 'root',
    })
], StringsService);



/***/ }),

/***/ 2468:
/*!******************************************!*\
  !*** ./src/app/services/data.service.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DataService": () => (/* binding */ DataService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);


const destionations = __webpack_require__(/*! ../data/destinations.json */ 442);
const home_categories = __webpack_require__(/*! ../data/home_categories.json */ 1110);
const trips = __webpack_require__(/*! ../data/trips.json */ 4156);
const hotels = __webpack_require__(/*! ../data/hotels.json */ 4437);
const flights = __webpack_require__(/*! ../data/flights.json */ 2943);
const cards = __webpack_require__(/*! ../data/cards.json */ 2304);
let DataService = class DataService {
    getDestinations() {
        return destionations;
    }
    getHomeCategories() {
        return home_categories;
    }
    getTrips() {
        return trips;
    }
    getHotels() {
        return hotels;
    }
    getFlights() {
        return flights;
    }
    getCards() {
        return cards;
    }
};
DataService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)({
        providedIn: 'root',
    })
], DataService);



/***/ }),

/***/ 8519:
/*!**************************************************!*\
  !*** ./src/app/services/geolocations.service.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GeolocationsService": () => (/* binding */ GeolocationsService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ 7152);



let GeolocationsService = class GeolocationsService {
    constructor(geolocation) {
        this.geolocation = geolocation;
    }
    getCoordsForGeoAddress(address, _default = true) {
        var self = this;
        return new Promise(resolve => {
            var self = this;
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        var loc = results[0].geometry.location;
                        var lat = loc.lat();
                        var lng = loc.lng();
                        resolve({ lat: lat, lng: lng });
                    }
                    else {
                        resolve(null);
                    }
                }
                else {
                    console.log({ results, status });
                    resolve(null);
                }
            });
        });
    }
    getCoordsViaHTML5Navigator() {
        return new Promise((resolve) => {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    resolve(pos);
                }, function () {
                    resolve({ lat: 51.5074, lng: 0.1278 });
                });
            }
            else {
                // Browser doesn't support Geolocation
                resolve({ lat: 51.5074, lng: 0.1278 });
            }
        });
    }
    getCurrentLocationCoordinates() {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            let coords = yield this.geolocation.getCurrentPosition();
            var lt = coords.coords.latitude;
            var lg = coords.coords.longitude;
            resolve({ lat: lt, lng: lg });
        }));
    }
};
GeolocationsService.ctorParameters = () => [
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_0__.Geolocation }
];
GeolocationsService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], GeolocationsService);



/***/ }),

/***/ 155:
/*!*************************************************!*\
  !*** ./src/app/services/interceptor.service.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InterceptorService": () => (/* binding */ InterceptorService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _utility_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utility.service */ 7278);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ 9412);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 3190);





let InterceptorService = class InterceptorService {
    constructor(utility) {
        this.utility = utility;
    }
    intercept(req, next) {
        return (0,rxjs__WEBPACK_IMPORTED_MODULE_1__.from)(this.callToken()).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.switchMap)(token => {
            const cloneRequest = this.addSecret(req, token);
            return next.handle(cloneRequest);
        }));
    }
    callToken() {
        return new Promise(resolve => {
            let token = localStorage.getItem('token');
            resolve(token);
            // this.sqlite.getCurrentUserAuthorizationToken().then( v => {
            //   // console.log("token must be here", v);
            //   resolve(v);
            // }).catch( err => {
            //   console.error(err);
            //   resolve(null)
            // });
        });
    }
    // private addSecret(request: HttpRequest<any>, value: any){
    //   let v = value ? value : '';
    //   let clone = request.clone({
    //     setHeaders : {
    //       Authorization: 'Bearer ' + v,
    //       Accept: "*",
    //       'Content-Type': "application/json",
    //       'X-Requested-With': 'XMLHttpRequest'
    //     }
    //   })
    //   return clone;
    // }
    addSecret(request, value) {
        let v = value ? value : localStorage.getItem('token');
        let obj = {
            Authorization: 'Bearer ' + v
        };
        obj['Accept'] = 'application/json';
        let cnt = request.headers.get('Content-Type');
        if (cnt == 'application/json') {
            obj['Content-Type'] = request.headers.get('Content-Type');
        }
        const clone = request.clone({
            setHeaders: obj
        });
        return clone;
    }
};
InterceptorService.ctorParameters = () => [
    { type: _utility_service__WEBPACK_IMPORTED_MODULE_0__.UtilityService }
];
InterceptorService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], InterceptorService);



/***/ }),

/***/ 2982:
/*!*********************************************!*\
  !*** ./src/app/services/network.service.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NetworkService": () => (/* binding */ NetworkService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./api.service */ 5830);
/* harmony import */ var _basic_events_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./basic/events.service */ 3511);
/* harmony import */ var _utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utility.service */ 7278);






let NetworkService = class NetworkService {
    constructor(utility, api, router, events) {
        this.utility = utility;
        this.api = api;
        this.router = router;
        this.events = events;
        this.serialize = ((obj) => {
            const str = [];
            for (const p in obj) {
                if (obj.hasOwnProperty(p)) {
                    str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
                }
            }
            return str.join('&');
        });
        // console.log('Hello NetworkProvider Provider');
    }
    httpPostResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
        return this.httpResponse('post', key, data, id, showloader, showError, contenttype);
    }
    httpGetResponse(key, id = null, showloader = false, showError = true, contenttype = 'application/json') {
        return this.httpResponse('get', key, {}, id, showloader, showError, contenttype);
    }
    httpPutResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
        return new Promise((resolve, reject) => {
            id = id ? `/${id}` : '';
            const url = key + id;
            this.api.put(key, data).subscribe((res) => {
                if (res.bool !== true) {
                    if (showError) {
                        this.utility.presentSuccessToast(res.message);
                    }
                    reject(null);
                }
                else {
                    resolve(res);
                }
            });
        });
    }
    httpPatchResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
        return new Promise((resolve, reject) => {
            id = id ? `/${id}` : '';
            const url = key + id;
            this.api.patch(key, data).subscribe((res) => {
                if (res.bool !== true) {
                    if (showError) {
                        this.utility.presentSuccessToast(res.message);
                    }
                    reject(null);
                }
                else {
                    resolve(res);
                }
            });
        });
    }
    httpDeleteResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
        return new Promise((resolve, reject) => {
            this.api.delete(key).subscribe((res) => {
                console.log(res);
                if (res.bool !== true) {
                    if (showError) {
                        this.utility.presentSuccessToast(res.message);
                    }
                    reject(null);
                }
                else {
                    resolve(res);
                }
            });
        });
    }
    // default 'Content-Type': 'application/json',
    httpResponse(type = 'get', key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
        return new Promise((resolve, reject) => {
            if (showloader === true) {
                this.utility.showLoader();
            }
            id = (id) ? '/' + id : '';
            const url = key + id;
            const seq = (type === 'get') ? this.api.get(url, {}) : this.api.post(url, data);
            seq.subscribe((res) => {
                if (showloader === true) {
                    this.utility.hideLoader();
                }
                if (res.success !== true) {
                    if (showError) {
                        this.utility.presentSuccessToast(res.message);
                    }
                    reject(null);
                }
                else {
                    resolve(res);
                }
            }, err => {
                const error = err.error;
                if (showloader === true) {
                    this.utility.hideLoader();
                }
                if (showError) {
                    this.utility.presentFailureToast(error.message);
                }
                console.log(err);
                // if(err.status === 401){
                //   this.router.navigate(['splash']);
                // }
                reject(null);
            });
        });
    }
    showFailure(err) {
        // console.error('ERROR', err);
        err = (err) ? err.message : 'check logs';
        this.utility.presentFailureToast(err);
    }
};
NetworkService.ctorParameters = () => [
    { type: _utility_service__WEBPACK_IMPORTED_MODULE_2__.UtilityService },
    { type: _api_service__WEBPACK_IMPORTED_MODULE_0__.ApiService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router },
    { type: _basic_events_service__WEBPACK_IMPORTED_MODULE_1__.EventsService }
];
NetworkService = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
        providedIn: 'root'
    })
], NetworkService);



/***/ }),

/***/ 3071:
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserService": () => (/* binding */ UserService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);


const users = __webpack_require__(/*! src/app/data/users.json */ 7244);
let UserService = class UserService {
    constructor() {
        this.user = null;
    }
    login(formdata) {
        return new Promise((resolve) => {
            let record = users.find((x) => x.email == formdata['email']);
            if (record) {
                this.setToken(record['id']);
                this.user = record;
                resolve(record);
            }
            else {
                this.user = null;
                resolve(false);
            }
        });
    }
    getUser() {
        return {
            name: 'Jont Henry',
            location: 'San Francisco, CA',
            email: 'jonthenery@gmail.com',
            phone: '(239) 555-0108',
            dob: 'March 27, 1989',
            address: '6391 Elgin, St celina, Delaware',
        };
        // return new Promise(async (resolve) => {
        //   let token = await this.getToken();
        //   console.log(token);
        //   let record = users.find((x) => parseInt(x.id) == parseInt(token));
        //   if (record) {
        //     this.user = record;
        //     resolve(record);
        //   } else {
        //     this.user = null;
        //     resolve(false);
        //   }
        // });
    }
    setToken(token) {
        return localStorage.setItem('token', token);
    }
    removeToken() {
        return localStorage.removeItem('token');
    }
    getToken() {
        return localStorage.getItem('token');
    }
};
UserService.ctorParameters = () => [];
UserService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)({
        providedIn: 'root',
    })
], UserService);



/***/ }),

/***/ 7278:
/*!*********************************************!*\
  !*** ./src/app/services/utility.service.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UtilityService": () => (/* binding */ UtilityService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _basic_alerts_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./basic/alerts.service */ 7661);
/* harmony import */ var _basic_loading_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./basic/loading.service */ 2537);
/* harmony import */ var _basic_strings_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./basic/strings.service */ 5030);
/* harmony import */ var _geolocations_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./geolocations.service */ 8519);
/* harmony import */ var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/launch-navigator/ngx */ 6297);







let UtilityService = class UtilityService {
    constructor(loading, alerts, strings, geolocations, launchNavigator) {
        this.loading = loading;
        this.alerts = alerts;
        this.strings = strings;
        this.geolocations = geolocations;
        this.launchNavigator = launchNavigator;
    }
    showLoader(msg = 'Please wait...') {
        return this.loading.showLoader(msg);
    }
    hideLoader() {
        return this.loading.hideLoader();
    }
    showAlert(msg) {
        return this.alerts.showAlert(msg);
    }
    presentToast(msg) {
        return this.alerts.presentToast(msg);
    }
    presentSuccessToast(msg) {
        return this.alerts.presentSuccessToast(msg);
    }
    presentFailureToast(msg) {
        return this.alerts.presentFailureToast(msg);
    }
    presentConfirm(okText = 'OK', cancelText = 'Cancel', title = 'Are You Sure?', message = '') {
        return this.alerts.presentConfirm(okText = okText, cancelText = cancelText, title = title, message = message);
    }
    onkeyupFormatPhoneNumberRuntime(phoneNumber, last = true) {
        return this.strings.formatPhoneNumberRuntime(phoneNumber);
    }
    /* Geolocations */
    openDirectionInMap(destination) {
        this.launchNavigator.navigate(destination);
    }
    getCoordsForGeoAddress(address, _default = true) {
        return this.geolocations.getCoordsForGeoAddress(address, _default = true);
    }
    getCoordsViaHTML5Navigator() {
        return this.geolocations.getCoordsViaHTML5Navigator();
    }
    getCurrentLocationCoordinates() {
        return this.geolocations.getCurrentLocationCoordinates();
    }
};
UtilityService.ctorParameters = () => [
    { type: _basic_loading_service__WEBPACK_IMPORTED_MODULE_1__.LoadingService },
    { type: _basic_alerts_service__WEBPACK_IMPORTED_MODULE_0__.AlertsService },
    { type: _basic_strings_service__WEBPACK_IMPORTED_MODULE_2__.StringsService },
    { type: _geolocations_service__WEBPACK_IMPORTED_MODULE_3__.GeolocationsService },
    { type: _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_4__.LaunchNavigator }
];
UtilityService = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Injectable)({
        providedIn: 'root'
    })
], UtilityService);



/***/ }),

/***/ 2340:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ 4431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ 4608);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 6747);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ 2340);




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
}
(0,_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__.platformBrowserDynamic)().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule)
    .catch(err => console.log(err));


/***/ }),

/***/ 863:
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/ lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \******************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./ion-action-sheet.entry.js": [
		7321,
		"common",
		"node_modules_ionic_core_dist_esm_ion-action-sheet_entry_js"
	],
	"./ion-alert.entry.js": [
		6108,
		"common",
		"node_modules_ionic_core_dist_esm_ion-alert_entry_js"
	],
	"./ion-app_8.entry.js": [
		1489,
		"common",
		"node_modules_ionic_core_dist_esm_ion-app_8_entry_js"
	],
	"./ion-avatar_3.entry.js": [
		305,
		"common",
		"node_modules_ionic_core_dist_esm_ion-avatar_3_entry_js"
	],
	"./ion-back-button.entry.js": [
		279,
		"common",
		"node_modules_ionic_core_dist_esm_ion-back-button_entry_js"
	],
	"./ion-backdrop.entry.js": [
		7757,
		"node_modules_ionic_core_dist_esm_ion-backdrop_entry_js"
	],
	"./ion-button_2.entry.js": [
		9215,
		"common",
		"node_modules_ionic_core_dist_esm_ion-button_2_entry_js"
	],
	"./ion-card_5.entry.js": [
		6911,
		"common",
		"node_modules_ionic_core_dist_esm_ion-card_5_entry_js"
	],
	"./ion-checkbox.entry.js": [
		937,
		"common",
		"node_modules_ionic_core_dist_esm_ion-checkbox_entry_js"
	],
	"./ion-chip.entry.js": [
		8695,
		"common",
		"node_modules_ionic_core_dist_esm_ion-chip_entry_js"
	],
	"./ion-col_3.entry.js": [
		6034,
		"node_modules_ionic_core_dist_esm_ion-col_3_entry_js"
	],
	"./ion-datetime_3.entry.js": [
		8837,
		"common",
		"node_modules_ionic_core_dist_esm_ion-datetime_3_entry_js"
	],
	"./ion-fab_3.entry.js": [
		4195,
		"common",
		"node_modules_ionic_core_dist_esm_ion-fab_3_entry_js"
	],
	"./ion-img.entry.js": [
		1709,
		"node_modules_ionic_core_dist_esm_ion-img_entry_js"
	],
	"./ion-infinite-scroll_2.entry.js": [
		5931,
		"node_modules_ionic_core_dist_esm_ion-infinite-scroll_2_entry_js"
	],
	"./ion-input.entry.js": [
		4513,
		"common",
		"node_modules_ionic_core_dist_esm_ion-input_entry_js"
	],
	"./ion-item-option_3.entry.js": [
		8056,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item-option_3_entry_js"
	],
	"./ion-item_8.entry.js": [
		862,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item_8_entry_js"
	],
	"./ion-loading.entry.js": [
		7509,
		"common",
		"node_modules_ionic_core_dist_esm_ion-loading_entry_js"
	],
	"./ion-menu_3.entry.js": [
		6272,
		"common",
		"node_modules_ionic_core_dist_esm_ion-menu_3_entry_js"
	],
	"./ion-modal.entry.js": [
		1855,
		"common",
		"node_modules_ionic_core_dist_esm_ion-modal_entry_js"
	],
	"./ion-nav_2.entry.js": [
		8708,
		"common",
		"node_modules_ionic_core_dist_esm_ion-nav_2_entry_js"
	],
	"./ion-popover.entry.js": [
		3527,
		"common",
		"node_modules_ionic_core_dist_esm_ion-popover_entry_js"
	],
	"./ion-progress-bar.entry.js": [
		4694,
		"common",
		"node_modules_ionic_core_dist_esm_ion-progress-bar_entry_js"
	],
	"./ion-radio_2.entry.js": [
		9222,
		"common",
		"node_modules_ionic_core_dist_esm_ion-radio_2_entry_js"
	],
	"./ion-range.entry.js": [
		5277,
		"common",
		"node_modules_ionic_core_dist_esm_ion-range_entry_js"
	],
	"./ion-refresher_2.entry.js": [
		9921,
		"common",
		"node_modules_ionic_core_dist_esm_ion-refresher_2_entry_js"
	],
	"./ion-reorder_2.entry.js": [
		3122,
		"common",
		"node_modules_ionic_core_dist_esm_ion-reorder_2_entry_js"
	],
	"./ion-ripple-effect.entry.js": [
		1602,
		"node_modules_ionic_core_dist_esm_ion-ripple-effect_entry_js"
	],
	"./ion-route_4.entry.js": [
		5174,
		"common",
		"node_modules_ionic_core_dist_esm_ion-route_4_entry_js"
	],
	"./ion-searchbar.entry.js": [
		7895,
		"common",
		"node_modules_ionic_core_dist_esm_ion-searchbar_entry_js"
	],
	"./ion-segment_2.entry.js": [
		6164,
		"common",
		"node_modules_ionic_core_dist_esm_ion-segment_2_entry_js"
	],
	"./ion-select_3.entry.js": [
		592,
		"common",
		"node_modules_ionic_core_dist_esm_ion-select_3_entry_js"
	],
	"./ion-slide_2.entry.js": [
		7162,
		"node_modules_ionic_core_dist_esm_ion-slide_2_entry_js"
	],
	"./ion-spinner.entry.js": [
		1374,
		"common",
		"node_modules_ionic_core_dist_esm_ion-spinner_entry_js"
	],
	"./ion-split-pane.entry.js": [
		7896,
		"node_modules_ionic_core_dist_esm_ion-split-pane_entry_js"
	],
	"./ion-tab-bar_2.entry.js": [
		5043,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab-bar_2_entry_js"
	],
	"./ion-tab_2.entry.js": [
		7802,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab_2_entry_js"
	],
	"./ion-text.entry.js": [
		9072,
		"common",
		"node_modules_ionic_core_dist_esm_ion-text_entry_js"
	],
	"./ion-textarea.entry.js": [
		2191,
		"common",
		"node_modules_ionic_core_dist_esm_ion-textarea_entry_js"
	],
	"./ion-toast.entry.js": [
		801,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toast_entry_js"
	],
	"./ion-toggle.entry.js": [
		4355,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toggle_entry_js"
	],
	"./ion-virtual-scroll.entry.js": [
		431,
		"node_modules_ionic_core_dist_esm_ion-virtual-scroll_entry_js"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(() => {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(() => {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = () => (Object.keys(map));
webpackAsyncContext.id = 863;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 3069:
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".menu-list ion-title {\n  text-align: left;\n  padding-left: 45px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLGdCQUFBO0VBQ0Esa0JBQUE7QUFBSiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWVudS1saXN0e1xyXG4gIGlvbi10aXRsZXtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDQ1cHg7XHJcbiAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 8781:
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".heading {\n  font-size: 28px;\n  margin-bottom: 0;\n}\n\n.login {\n  --background: var(--ion-color-primary);\n}\n\n.gridStyle {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.loginFont {\n  color: rgba(255, 255, 255, 0.8);\n}\n\n.social-btn {\n  font-weight: bold;\n  color: black;\n}\n\n.passwordFont {\n  position: absolute;\n  top: 38.3%;\n  left: 11%;\n  color: white;\n  font-size: 9pt;\n  z-index: 10;\n  padding-left: 5px;\n  padding-right: 5px;\n  background-color: #222222;\n}\n\n.subheading {\n  font-size: 14px;\n  margin-top: 0;\n  margin-bottom: 0;\n}\n\n.marginTop {\n  margin-top: 10%;\n}\n\n.input {\n  --padding-start: 20px;\n  font-size: 13px;\n  height: 50px;\n  --placeholder-color: var(--ion-color-medium);\n  color: white;\n}\n\n.eye-icon-box-outer {\n  position: relative;\n}\n\n.eye-icon-box-outer .eye-icon-box {\n  position: absolute;\n  top: 29px;\n  right: 20px;\n  color: var(--ion-color-medium);\n  font-size: 20px;\n}\n\n.eye-icon-box-outer .passwordFont {\n  top: -5px;\n  left: 15px;\n  position: absolute;\n  color: rgba(255, 255, 255, 0.6);\n  font-size: 9pt;\n  z-index: 10;\n  padding-left: 5px;\n  padding-right: 5px;\n  background-color: var(--ion-color-primary);\n}\n\n.email-box-outer {\n  position: relative;\n}\n\n.email-box-outer .emailFont {\n  bottom: 35px;\n  left: 15px;\n  position: absolute;\n  color: rgba(255, 255, 255, 0.6);\n  font-size: 9pt;\n  z-index: 10;\n  padding-left: 5px;\n  padding-right: 5px;\n  background-color: var(--ion-color-primary);\n}\n\n.btn {\n  height: 48px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtBQUNGOztBQUNBO0VBQ0Usc0NBQUE7QUFFRjs7QUFBQTtFQUNFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBR0Y7O0FBREE7RUFDRSwrQkFBQTtBQUlGOztBQURBO0VBQ0UsaUJBQUE7RUFDQSxZQUFBO0FBSUY7O0FBU0E7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0FBTkY7O0FBUUE7RUFDRSxlQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0FBTEY7O0FBT0E7RUFDRSxlQUFBO0FBSkY7O0FBT0E7RUFDRSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsNENBQUE7RUFDQSxZQUFBO0FBSkY7O0FBT0E7RUFDRSxrQkFBQTtBQUpGOztBQUtFO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsZUFBQTtBQUhKOztBQUtFO0VBRUUsU0FBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLCtCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsMENBQUE7QUFKSjs7QUFPQTtFQUNFLGtCQUFBO0FBSkY7O0FBS0U7RUFDRSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsK0JBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQ0FBQTtBQUhKOztBQU9BO0VBQ0UsWUFBQTtBQUpGIiwiZmlsZSI6ImxvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkaW5nIHtcclxuICBmb250LXNpemU6IDI4cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMDtcclxufVxyXG4ubG9naW4ge1xyXG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG59XHJcbi5ncmlkU3R5bGUge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4ubG9naW5Gb250IHtcclxuICBjb2xvcjogcmdiYSgkY29sb3I6ICNmZmYsICRhbHBoYTogMC44KTtcclxuICAvLyBmb250LXdlaWdodDogODAwO1xyXG59XHJcbi5zb2NpYWwtYnRuIHtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuLy8gLmVtYWlsRm9udCB7XHJcbi8vICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4vLyAgIHRvcDogMzAuNSU7XHJcbi8vICAgbGVmdDogMTElO1xyXG4vLyAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICBmb250LXNpemU6IDlwdDtcclxuLy8gICB6LWluZGV4OiAxMDtcclxuLy8gICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuLy8gICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbi8vICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4vLyB9XHJcbi5wYXNzd29yZEZvbnQge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDM4LjMlO1xyXG4gIGxlZnQ6IDExJTtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgZm9udC1zaXplOiA5cHQ7XHJcbiAgei1pbmRleDogMTA7XHJcbiAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgcGFkZGluZy1yaWdodDogNXB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMyMjIyMjI7XHJcbn1cclxuLnN1YmhlYWRpbmcge1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBtYXJnaW4tdG9wOiAwO1xyXG4gIG1hcmdpbi1ib3R0b206IDA7XHJcbn1cclxuLm1hcmdpblRvcCB7XHJcbiAgbWFyZ2luLXRvcDogMTAlO1xyXG59XHJcblxyXG4uaW5wdXQge1xyXG4gIC0tcGFkZGluZy1zdGFydDogMjBweDtcclxuICBmb250LXNpemU6IDEzcHg7XHJcbiAgaGVpZ2h0OiA1MHB4O1xyXG4gIC0tcGxhY2Vob2xkZXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmV5ZS1pY29uLWJveC1vdXRlciB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIC5leWUtaWNvbi1ib3gge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAyOXB4O1xyXG4gICAgcmlnaHQ6IDIwcHg7XHJcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgfVxyXG4gIC5wYXNzd29yZEZvbnQge1xyXG4gICAgLy8gYm90dG9tOiAxMDBweDtcclxuICAgIHRvcDogLTVweDtcclxuICAgIGxlZnQ6IDE1cHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBjb2xvcjogcmdiYSgkY29sb3I6ICNmZmYsICRhbHBoYTogMC42KTtcclxuICAgIGZvbnQtc2l6ZTogOXB0O1xyXG4gICAgei1pbmRleDogMTA7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICB9XHJcbn1cclxuLmVtYWlsLWJveC1vdXRlciB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIC5lbWFpbEZvbnQge1xyXG4gICAgYm90dG9tOiAzNXB4O1xyXG4gICAgbGVmdDogMTVweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGNvbG9yOiByZ2JhKCRjb2xvcjogI2ZmZiwgJGFscGhhOiAwLjYpO1xyXG4gICAgZm9udC1zaXplOiA5cHQ7XHJcbiAgICB6LWluZGV4OiAxMDtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgcGFkZGluZy1yaWdodDogNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gIH1cclxufVxyXG5cclxuLmJ0biB7XHJcbiAgaGVpZ2h0OiA0OHB4O1xyXG59XHJcblxyXG4vLyAucGQge1xyXG4vLyAgIHBhZGRpbmc6IDEwcHg7XHJcbi8vIH1cclxuIl19 */");

/***/ }),

/***/ 9233:
/*!***********************************************!*\
  !*** ./src/app/pages/signup/signup.page.scss ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".heading {\n  font-size: 28px;\n  margin-bottom: 0;\n}\n\n.subheading {\n  font-size: 14px;\n  margin-top: 0;\n  margin-bottom: 0;\n}\n\n.signup {\n  --background: var(--ion-color-primary);\n}\n\n.signUp {\n  color: \"tertiary\";\n}\n\n.input {\n  --padding-start: 20px;\n  font-size: 13px;\n  height: 50px;\n  color: white;\n  --placeholder-color: #8d8d8d;\n}\n\n.marginTop {\n  margin-top: 10%;\n}\n\n.namePosition {\n  position: absolute;\n  top: 29%;\n  left: 8%;\n}\n\n.emailPosition {\n  position: absolute;\n  top: 36.8%;\n  left: 8%;\n}\n\n.phonePosition {\n  position: absolute;\n  top: 44.6%;\n  left: 8%;\n}\n\n.dobPosition {\n  position: absolute;\n  top: 52.6%;\n  left: 8%;\n}\n\n.passwordPosition {\n  position: absolute;\n  top: 60.4%;\n  left: 8%;\n}\n\n.conPasPosition {\n  position: absolute;\n  top: 68.2%;\n  left: 8%;\n}\n\n.Font {\n  color: white;\n  font-size: 9pt;\n  z-index: 10;\n  padding-left: 5px;\n  padding-right: 5px;\n  height: 25px;\n  background-color: var(--ion-color-primary);\n}\n\n.eye-icon-box-outer {\n  position: relative;\n}\n\n.eye-icon-box-outer .eye-icon-box {\n  position: absolute;\n  top: 29px;\n  right: 20px;\n  color: var(--ion-color-medium);\n  font-size: 20px;\n}\n\n.eye-icon-box-outer .title-password-box {\n  bottom: 35px;\n  left: 15px;\n  position: absolute;\n  color: white;\n  font-size: 9pt;\n  z-index: 10;\n  padding-left: 5px;\n  padding-right: 5px;\n  background-color: var(--ion-color-primary);\n}\n\n.box-outer {\n  position: relative;\n}\n\n.box-outer .title-box {\n  bottom: 35px;\n  left: 15px;\n  position: absolute;\n  color: rgba(255, 255, 255, 0.6);\n  font-size: 9pt;\n  z-index: 10;\n  padding-left: 5px;\n  padding-right: 5px;\n  background-color: var(--ion-color-primary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNpZ251cC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7QUFDRjs7QUFFQTtFQUNFLGVBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUFDRjs7QUFDQTtFQUNFLHNDQUFBO0FBRUY7O0FBQUE7RUFDRSxpQkFBQTtBQUdGOztBQUFBO0VBRUUscUJBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSw0QkFBQTtBQUVGOztBQUFBO0VBQ0UsZUFBQTtBQUdGOztBQURBO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtBQUlGOztBQUZBO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtBQUtGOztBQUhBO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtBQU1GOztBQUpBO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtBQU9GOztBQUxBO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtBQVFGOztBQU5BO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtBQVNGOztBQVBBO0VBQ0UsWUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFHQSwwQ0FBQTtBQVFGOztBQU5BO0VBQ0Usa0JBQUE7QUFTRjs7QUFSRTtFQUNFLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7QUFVSjs7QUFSRTtFQUNFLFlBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsMENBQUE7QUFVSjs7QUFQQTtFQUNFLGtCQUFBO0FBVUY7O0FBVEU7RUFDRSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsK0JBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQ0FBQTtBQVdKIiwiZmlsZSI6InNpZ251cC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGluZyB7XHJcbiAgZm9udC1zaXplOiAyOHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDA7XHJcbn1cclxuXHJcbi5zdWJoZWFkaW5nIHtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgbWFyZ2luLXRvcDogMDtcclxuICBtYXJnaW4tYm90dG9tOiAwO1xyXG59XHJcbi5zaWdudXAge1xyXG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG59XHJcbi5zaWduVXAge1xyXG4gIGNvbG9yOiBcInRlcnRpYXJ5XCI7XHJcbn1cclxuXHJcbi5pbnB1dCB7XHJcbiAgLy8gLS1wYWRkaW5nLXRvcDogMTBweDtcclxuICAtLXBhZGRpbmctc3RhcnQ6IDIwcHg7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIGhlaWdodDogNTBweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjogIzhkOGQ4ZDtcclxufVxyXG4ubWFyZ2luVG9wIHtcclxuICBtYXJnaW4tdG9wOiAxMCU7XHJcbn1cclxuLm5hbWVQb3NpdGlvbiB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMjklO1xyXG4gIGxlZnQ6IDglO1xyXG59XHJcbi5lbWFpbFBvc2l0aW9uIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAzNi44JTtcclxuICBsZWZ0OiA4JTtcclxufVxyXG4ucGhvbmVQb3NpdGlvbiB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogNDQuNiU7XHJcbiAgbGVmdDogOCU7XHJcbn1cclxuLmRvYlBvc2l0aW9uIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiA1Mi42JTtcclxuICBsZWZ0OiA4JTtcclxufVxyXG4ucGFzc3dvcmRQb3NpdGlvbiB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogNjAuNCU7XHJcbiAgbGVmdDogOCU7XHJcbn1cclxuLmNvblBhc1Bvc2l0aW9uIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiA2OC4yJTtcclxuICBsZWZ0OiA4JTtcclxufVxyXG4uRm9udCB7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZvbnQtc2l6ZTogOXB0O1xyXG4gIHotaW5kZXg6IDEwO1xyXG4gIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcclxuICBoZWlnaHQ6IDI1cHg7XHJcbiAgLy8gbWFyZ2luLXRvcDogLTJweDtcclxuICAvLyAtLWJhY2tncm91bmQ6IFwicHJpbWFyeVwiO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxufVxyXG4uZXllLWljb24tYm94LW91dGVyIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgLmV5ZS1pY29uLWJveCB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDI5cHg7XHJcbiAgICByaWdodDogMjBweDtcclxuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICB9XHJcbiAgLnRpdGxlLXBhc3N3b3JkLWJveCB7XHJcbiAgICBib3R0b206IDM1cHg7XHJcbiAgICBsZWZ0OiAxNXB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiA5cHQ7XHJcbiAgICB6LWluZGV4OiAxMDtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgcGFkZGluZy1yaWdodDogNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gIH1cclxufVxyXG4uYm94LW91dGVyIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgLnRpdGxlLWJveCB7XHJcbiAgICBib3R0b206IDM1cHg7XHJcbiAgICBsZWZ0OiAxNXB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgY29sb3I6IHJnYmEoJGNvbG9yOiAjZmZmLCAkYWxwaGE6IDAuNik7XHJcbiAgICBmb250LXNpemU6IDlwdDtcclxuICAgIHotaW5kZXg6IDEwO1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgfVxyXG59XHJcblxyXG4vLyAuYnRuIHtcclxuLy8gICBoZWlnaHQ6IDYwcHg7XHJcbi8vICAgZm9udC13ZWlnaHQ6IDkwMDtcclxuLy8gICBmb250LXNpemU6IDEuMnJlbTtcclxuLy8gfVxyXG4iXX0= */");

/***/ }),

/***/ 1106:
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-menu menuId=\"main\" contentId=\"main\">\r\n\r\n  <ion-header>\r\n    <ion-toolbar color=\"medium\">\r\n      <ion-title>Flock Travel</ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n\r\n  <ion-content color=\"primary\">\r\n    <ion-list class=\"menu-list ion-no-padding\">\r\n      <ion-item color=\"primary\" (click)=\"goTo('explore')\">\r\n        <ion-icon name=\"compass-outline\"></ion-icon>\r\n        <ion-title>\r\n          Explore\r\n        </ion-title>\r\n      </ion-item>\r\n      <ion-item color=\"primary\" (click)=\"goTo('trips')\">\r\n        <ion-icon name=\"globe-outline\"></ion-icon>\r\n        <ion-title>\r\n          Trips\r\n        </ion-title>\r\n      </ion-item>\r\n      <ion-item color=\"primary\" (click)=\"goTo('saved')\">\r\n        <ion-icon name=\"heart-outline\"></ion-icon>\r\n        <ion-title>\r\n          Saved\r\n        </ion-title>\r\n      </ion-item>\r\n      <ion-item color=\"primary\" (click)=\"goTo('messages')\">\r\n        <ion-icon name=\"cloud-upload-outline\"></ion-icon>\r\n        <ion-title>\r\n          Inbox\r\n        </ion-title>\r\n      </ion-item>\r\n\r\n      <!-- <ion-item (click)=\"goTo('profile')\">\r\n        <ion-icon name=\"person-outline\"></ion-icon>\r\n        <ion-title>\r\n          Profile\r\n        </ion-title>\r\n      </ion-item> -->\r\n\r\n      <ion-item color=\"primary\" (click)=\"goTo('profile')\">\r\n        <ion-icon name=\"person-outline\"></ion-icon>\r\n        <ion-title>\r\n          Profile\r\n        </ion-title>\r\n      </ion-item>  \r\n\r\n      <!-- <ion-item (click)=\"goTo('change-password')\">\r\n        <ion-icon name=\"person-outline\"></ion-icon>\r\n        <ion-title>\r\n          Change Password\r\n        </ion-title>\r\n      </ion-item> -->\r\n\r\n      <!-- <ion-item (click)=\"logOut()\">\r\n        <ion-title>\r\n          Log out\r\n        </ion-title>\r\n      </ion-item> -->\r\n    </ion-list>\r\n  </ion-content>\r\n</ion-menu>\r\n\r\n<ion-app id=\"main\">\r\n  <ion-router-outlet></ion-router-outlet>\r\n</ion-app>\r\n");

/***/ }),

/***/ 1021:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- <ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-buttons>\r\n      <ion-button (click)=\"nav.pop()\">\r\n        <ion-icon color=\"medium\" name=\"arrow-back-circle\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header> -->\r\n<ion-content class=\"login\">\r\n  <ion-grid class=\"ion-margin\">\r\n    <ion-row class=\"ion-text-center marginTop\">\r\n      <ion-col>\r\n        <img src=\"assets/applogo.png\" />\r\n        <!-- <p class=\"f-pop-r subheading\">\r\n          <ion-text color=\"medium\">Login</ion-text>\r\n        </p> -->\r\n        <h3 class=\"loginFont\">Login</h3>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <!-- <div class=\"emailFont\">\r\n      <p>Email</p>\r\n    </div>\r\n    <div class=\"passwordFont\">\r\n      <p>Password</p>\r\n    </div> -->\r\n\r\n    <form [formGroup]=\"aForm\">\r\n      <ion-row>\r\n        <ion-col size=\"12\" class=\"email-box-outer\">\r\n          <p class=\"emailFont\">Email</p>\r\n          <ion-input\r\n            formControlName=\"email\"\r\n            class=\"input f-pop-li\"\r\n            placeholder=\"Email Address\"\r\n          ></ion-input>\r\n        </ion-col>\r\n      </ion-row>\r\n\r\n      <ion-row>\r\n        <ion-col size=\"12\" class=\"eye-icon-box-outer\">\r\n          <p class=\"passwordFont\">Password</p>\r\n          <ion-input\r\n            formControlName=\"password\"\r\n            type=\"password\"\r\n            class=\"input f-pop-li\"\r\n            placeholder=\"Password\"\r\n          >\r\n          </ion-input>\r\n          <ion-icon class=\"eye-icon-box\" name=\"eye-outline\"></ion-icon>\r\n        </ion-col>\r\n      </ion-row>\r\n    </form>\r\n\r\n    <ion-row>\r\n      <ion-col class=\"ion-text-center\">\r\n        <ion-button expand=\"block\" color=\"secondary\" fill=\"clear\"\r\n          >Forgot password?</ion-button\r\n        >\r\n        <!-- <ion-text color=\"primary\" class=\"f-pop-m\"> Click here </ion-text> -->\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-button\r\n          class=\"btn\"\r\n          expand=\"block\"\r\n          color=\"secondary\"\r\n          (click)=\"login()\"\r\n        >\r\n          <ion-text class=\"pd\"> Login </ion-text>\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <!-- <div style=\"height: 30px\"></div> -->\r\n\r\n    <ion-row class=\"social-div ion-margin-top\">\r\n      <ion-col class=\"ion-text-center\">\r\n        <ion-text color=\"medium\" style=\"color: #cccccc\">Or</ion-text>\r\n        <br />\r\n        <!-- <ion-text class=\"f-pop-m\">Continue With Social Media</ion-text> -->\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <!-- <ion-row>\r\n      <ion-col size=\"12\">\r\n        <ion-button\r\n          expand=\"block\"\r\n          class=\"social-btn\"\r\n          style=\"--background: #395895\"\r\n          shape=\"round\"\r\n        >\r\n          <ion-icon name=\"logo-facebook\"></ion-icon>\r\n          <span class=\"ion-margin-horizontal\"> | </span>\r\n          Login With Facebook\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row> -->\r\n\r\n    <ion-row class=\"ion-margin-top\">\r\n      <ion-col size=\"12\">\r\n        <ion-button expand=\"block\" class=\"btn social-btn\" color=\"light\">\r\n          <ion-img\r\n            class=\"ion-margin-right\"\r\n            src=\"assets/google.png\"\r\n            style=\"height: 35px\"\r\n          ></ion-img>\r\n          <ion-text class=\"pd\"> Login With Google </ion-text>\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <div>\r\n      <ion-row>\r\n        <ion-col class=\"ion-text-center\">\r\n          <ion-button (click)=\"openSignup()\" fill=\"clear\">\r\n            <ion-text class=\"f-pop-m\" style=\"font-size: 14px\" color=\"light\">\r\n              Don't Have an Account?\r\n            </ion-text>\r\n            <ion-text\r\n              class=\"f-pop-m\"\r\n              style=\"font-size: 14px; margin-left: 5px\"\r\n              color=\"secondary\"\r\n            >\r\n              Sign Up\r\n            </ion-text>\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </div>\r\n  </ion-grid>\r\n</ion-content>\r\n");

/***/ }),

/***/ 1979:
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/signup/signup.page.html ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- <ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <ion-buttons>\r\n      <ion-button (click)=\"nav.pop()\">\r\n        <ion-icon color=\"medium\" name=\"arrow-back-circle\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header> -->\r\n\r\n<ion-content class=\"signup\">\r\n  <ion-grid class=\"marginTop ion-padding ion-margin\">\r\n    <ion-row class=\"ion-text-center\">\r\n      <ion-col>\r\n        <img src=\"assets/applogo.png\" />\r\n        <!-- <p class=\"f-pop-r subheading\">\r\n          <ion-text color=\"medium\">Add Your Details To Create Account</ion-text>\r\n        </p> -->\r\n        <h3 class=\"signUpFont\" style=\"color: white\">Sign Up</h3>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <!-- <div class=\"Font namePosition\">\r\n      <p>Name</p>\r\n    </div>\r\n    <div class=\"Font emailPosition\">\r\n      <p>Email</p>\r\n    </div>\r\n    <div class=\"Font phonePosition\">\r\n      <p>Phone</p>\r\n    </div>\r\n    <div class=\"Font dobPosition\">\r\n      <p>Date of Birth</p>\r\n    </div>\r\n    <div class=\"Font passwordPosition\">\r\n      <p>Password</p>\r\n    </div>\r\n    <div class=\"Font conPasPosition\">\r\n      <p>Confirm Password</p>\r\n    </div> -->\r\n\r\n    <ion-row>\r\n      <ion-col size=\"12\" class=\"box-outer\">\r\n        <p class=\"title-box\">Name</p>\r\n        <ion-input placeholder=\"Jhon Epam\" class=\"input\"></ion-input>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col size=\"12\" class=\"box-outer\">\r\n        <p class=\"title-box\">Email</p>\r\n        <ion-input placeholder=\"example@gmail.com\" class=\"input\"></ion-input>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col size=\"12\" class=\"box-outer\">\r\n        <p class=\"title-box\">Phone</p>\r\n        <ion-input placeholder=\"Phone Number\" class=\"input\"></ion-input>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col size=\"12\" class=\"box-outer\">\r\n        <p class=\"title-box\">Date of Birth</p>\r\n        <ion-input placeholder=\"mm/dd/yyyy\" class=\"input\"></ion-input>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col size=\"12\" class=\"eye-icon-box-outer\">\r\n        <p class=\"title-password-box\">Password</p>\r\n        <ion-input\r\n          placeholder=\"Password\"\r\n          type=\"password\"\r\n          class=\"input\"\r\n        ></ion-input>\r\n        <ion-icon class=\"eye-icon-box\" name=\"eye-outline\"></ion-icon>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col size=\"12\" class=\"eye-icon-box-outer\">\r\n        <p class=\"title-password-box\">Confirm Password</p>\r\n        <ion-input placeholder=\"Password\" class=\"input\"></ion-input>\r\n        <ion-icon class=\"eye-icon-box\" name=\"eye-outline\"></ion-icon>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row class=\"ion-margin-top\">\r\n      <ion-col>\r\n        <ion-button expand=\"block\" color=\"secondary\" (click)=\"singUp()\">\r\n          Sign Up\r\n        </ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <div>\r\n      <ion-row>\r\n        <ion-col class=\"ion-text-center\">\r\n          <ion-button (click)=\"openLogin()\" fill=\"clear\">\r\n            <ion-text\r\n              class=\"f-pop-m\"\r\n              color=\"light\"\r\n              style=\"margin-right: 5px; font-size: 14px\"\r\n            >\r\n              Already Have an Account?\r\n            </ion-text>\r\n            <ion-text class=\"f-pop-m\" style=\"font-size: 14px\" color=\"secondary\">\r\n              Login\r\n            </ion-text>\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </div>\r\n  </ion-grid>\r\n</ion-content>\r\n");

/***/ }),

/***/ 2304:
/*!*********************************!*\
  !*** ./src/app/data/cards.json ***!
  \*********************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('[{"card_number":"1234 5678 9101 1236","card_holder":"TONY NGUYEN","expires":"04/23"},{"card_number":"1234 5678 9101 1236","card_holder":"JOHN DOE","expires":"12/25"}]');

/***/ }),

/***/ 442:
/*!****************************************!*\
  !*** ./src/app/data/destinations.json ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('[{"id":1,"name":"USA","selected":true,"location":"New York","image":"assets/Images/USA.jpg"},{"id":2,"name":"DUBAI","selected":false,"location":"The Palm Jumeriah","image":"assets/Images/DUBAI.jpg"},{"id":3,"name":"AUSTRAILIA","selected":false,"location":"Sydney","image":"assets/Images/AUSTRAILIA.jpg"},{"id":4,"name":"INDIA","selected":true,"location":"Taj Mahal","image":"assets/Images/INDIA.jpg"},{"id":5,"name":"England","selected":true,"location":"London","image":"assets/Images/USA.jpg"},{"id":6,"name":"Thailand","location":"Bangkok","selected":false,"image":"assets/Images/DUBAI.jpg"},{"id":7,"name":"Italy","location":"Rome","selected":true,"image":"assets/Images/USA.jpg"},{"id":8,"name":"China","location":"Macau","selected":false,"image":"assets/Images/DUBAI.jpg"},{"id":9,"name":"Turkey","location":"Istanbul","selected":false,"image":"assets/Images/DUBAI.jpg"}]');

/***/ }),

/***/ 2943:
/*!***********************************!*\
  !*** ./src/app/data/flights.json ***!
  \***********************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('[{"quality":"FASTEST","time":"23:45 - 04:30","fair":"$867","duration":"15h 15m - Direct","flight":"United Airlines UA 802","from":"Rhodes, California, United States","to":"Abu Dhabi, Dubai.","departure":"23:45, Fri, 08 May","arrival":"04:30, Sat, 09 May","codes":"COL - DUB"},{"quality":"CHEAPEST","time":"01:55 - 07:00","fair":"$550","duration":"15h 35m - Direct","flight":"Air India AI 101","from":"Rhodes, California, United States","to":"Abu Dhabi, Dubai.","departure":"23:45, Fri, 08 May","arrival":"04:30, Sat, 09 May","codes":"COL - DUB"},{"quality":"","time":"02:15 - 07:25","fair":"$867","duration":"15h 25m - Direct","flight":"Turkish Airlines","from":"Rhodes, California, United States","to":"Abu Dhabi, Dubai.","departure":"23:45, Fri, 08 May","arrival":"04:30, Sat, 09 May","codes":"COL - DUB"},{"quality":"","time":"02:15 - 07:25","fair":"$600","duration":"15h 25m - Direct","flight":"Malaysia Airlines","from":"Rhodes, California, United States","to":"Abu Dhabi, Dubai.","departure":"23:45, Fri, 08 May","arrival":"04:30, Sat, 09 May","codes":"COL - DUB"}]');

/***/ }),

/***/ 1110:
/*!*******************************************!*\
  !*** ./src/app/data/home_categories.json ***!
  \*******************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('[{"id":1,"name":"Trend","selected":true},{"id":2,"name":"Featured","selected":false},{"id":3,"name":"Top Visit","selected":false}]');

/***/ }),

/***/ 4437:
/*!**********************************!*\
  !*** ./src/app/data/hotels.json ***!
  \**********************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('[{"id":1,"name":"White Ibzia","rate":"$13.44","rating":5,"image":"../assets/Images/hotel1.jpg"}]');

/***/ }),

/***/ 4156:
/*!*********************************!*\
  !*** ./src/app/data/trips.json ***!
  \*********************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('[{"id":1,"title":"Upcoming Trip","location":"Abu Dhabi, Dubai","date_time":"Fri, 08 May - Sat, 17 May","description":"HOTEL RESERVATION AND FLIGHT BOOKED"}]');

/***/ }),

/***/ 7244:
/*!*********************************!*\
  !*** ./src/app/data/users.json ***!
  \*********************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('[{"id":1,"username":"John Smith","email":"john_smith22@gmail.com","password":"123456","gender":"Male","dob":"1994-04-12","location":"7812 Coopers Hawk Trail, Machesney Park, IL 61115, USA","number":23,"latitude":44.163451584711275,"longitude":-89.45129309578454,"image":"assets/users/user1.jpg","country":"","home_town":"","relations":{"racing_series_id":"1,2,3,4","role_id":"3"}},{"id":2,"username":"Michele Abbate","email":"michele_abbate@gmail.com","password":"123456","gender":"Male","dob":"1994-04-12","location":"","number":28,"latitude":0,"longitude":0,"image":"assets/users/user2.jpg","country":"","home_town":"","relations":{"racing_series_id":"1,2,3,4","role_id":"3"}}]');

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ "use strict";
/******/ 
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(4431)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map
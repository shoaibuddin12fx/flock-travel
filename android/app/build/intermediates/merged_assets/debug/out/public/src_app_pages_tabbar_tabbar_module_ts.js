(self["webpackChunkVeenme"] = self["webpackChunkVeenme"] || []).push([["src_app_pages_tabbar_tabbar_module_ts"],{

/***/ 3867:
/*!*******************************************************!*\
  !*** ./src/app/pages/tabbar/tabbar-routing.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TabbarPageRoutingModule": () => (/* binding */ TabbarPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _tabbar_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tabbar.page */ 4833);




const routes = [
    {
        path: '',
        component: _tabbar_page__WEBPACK_IMPORTED_MODULE_0__.TabbarPage,
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'explore',
            },
            {
                path: 'explore',
                loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_explore_explore_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ../explore/explore.module */ 8227)).then((m) => m.ExplorePageModule),
            },
            {
                path: 'saved',
                loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_saved_saved_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ../saved/saved.module */ 7239)).then(m => m.SavedPageModule)
            },
            {
                path: 'inbox',
                loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_inbox_inbox_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ../inbox/inbox.module */ 8580)).then(m => m.InboxPageModule)
            },
            {
                path: 'trips',
                loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_trips_trips_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ../trips/trips.module */ 4629)).then((m) => m.TripsPageModule),
            },
            {
                path: 'profile',
                loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_pages_edit-profile_edit-profile_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./../edit-profile/edit-profile.module */ 483)).then((m) => m.EditProfilePageModule),
            },
        ]
        // {
        //   path: 'races',
        //   loadChildren: () =>
        //     import('./../races/races.module').then((m) => m.RacesPageModule),
        // },
        // {
        //   path: 'news',
        //   loadChildren: () =>
        //     import('./../news/news.module').then((m) => m.NewsPageModule),
        // },
        // {
        //   path: 'timetable',
        //   loadChildren: () =>
        //     import('./../timetable/timetable.module').then(
        //       (m) => m.TimetablePageModule
        //     ),
        // },
        // {
        //   path: 'videos',
        //   loadChildren: () =>
        //     import('./../videos/videos.module').then((m) => m.VideosPageModule),
        // },
        // {
        //   path: 'store',
        //   loadChildren: () =>
        //     import('./../store/store.module').then((m) => m.StorePageModule),
        // },
    },
];
let TabbarPageRoutingModule = class TabbarPageRoutingModule {
};
TabbarPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], TabbarPageRoutingModule);



/***/ }),

/***/ 9920:
/*!***********************************************!*\
  !*** ./src/app/pages/tabbar/tabbar.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TabbarPageModule": () => (/* binding */ TabbarPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _tabbar_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tabbar-routing.module */ 3867);
/* harmony import */ var _tabbar_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tabbar.page */ 4833);







let TabbarPageModule = class TabbarPageModule {
};
TabbarPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _tabbar_routing_module__WEBPACK_IMPORTED_MODULE_0__.TabbarPageRoutingModule
        ],
        declarations: [_tabbar_page__WEBPACK_IMPORTED_MODULE_1__.TabbarPage]
    })
], TabbarPageModule);



/***/ }),

/***/ 4833:
/*!*********************************************!*\
  !*** ./src/app/pages/tabbar/tabbar.page.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TabbarPage": () => (/* binding */ TabbarPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_tabbar_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./tabbar.page.html */ 802);
/* harmony import */ var _tabbar_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tabbar.page.scss */ 2117);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 4282);
var TabbarPage_1;





// import { HomePage } from '../home/home.page';
let TabbarPage = TabbarPage_1 = class TabbarPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
    }
    ngOnInit() {
    }
    setCurrentTab() {
        TabbarPage_1.selectedTab = this.tabs.getSelected();
    }
};
TabbarPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
TabbarPage.propDecorators = {
    tabs: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.ViewChild, args: ['tabs', { static: false },] }]
};
TabbarPage = TabbarPage_1 = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-tabbar',
        template: _raw_loader_tabbar_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_tabbar_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], TabbarPage);



/***/ }),

/***/ 2117:
/*!***********************************************!*\
  !*** ./src/app/pages/tabbar/tabbar.page.scss ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-tab-bar {\n  height: 75px;\n}\n\nion-tab-button {\n  --color-selected: var(--ion-color-primary);\n}\n\n.center-icon {\n  background: red;\n  padding: 10px;\n  border: 1px solid red;\n  border-radius: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRhYmJhci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0FBQ0Y7O0FBS0E7RUFDRSwwQ0FBQTtBQUZGOztBQUtBO0VBQ0UsZUFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0FBRkYiLCJmaWxlIjoidGFiYmFyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10YWItYmFyIHtcclxuICBoZWlnaHQ6IDc1cHg7XHJcbiAgLy8gLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuXHJcblxyXG59XHJcblxyXG5pb24tdGFiLWJ1dHRvbiB7XHJcbiAgLS1jb2xvci1zZWxlY3RlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG59XHJcblxyXG4uY2VudGVyLWljb24ge1xyXG4gIGJhY2tncm91bmQ6IHJlZDtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIHJlZDtcclxuICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 802:
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabbar/tabbar.page.html ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-tabs #tabs (ionTabsDidChange)=\"setCurrentTab()\">\r\n  <ion-tab-bar color=\"primary\" selectedTab=\"explore\" slot=\"bottom\">\r\n    <ion-tab-button tab=\"explore\">\r\n      <ion-icon name=\"compass-outline\"></ion-icon>\r\n      <ion-label>Explore</ion-label>\r\n      <!-- <ion-badge>6</ion-badge> -->\r\n    </ion-tab-button>\r\n\r\n    <ion-tab-button tab=\"trips\">\r\n      <ion-icon name=\"globe-outline\"></ion-icon>\r\n      <ion-label>Trips</ion-label>\r\n      <!-- <ion-badge>6</ion-badge> -->\r\n    </ion-tab-button>\r\n\r\n    <ion-tab-button tab=\"saved\">\r\n      <ion-icon name=\"heart-outline\"></ion-icon>\r\n      <ion-label>Saved</ion-label>\r\n    </ion-tab-button>\r\n\r\n    <ion-tab-button>\r\n      <ion-icon name=\"cloud-upload-outline\"></ion-icon>\r\n      <ion-label>Inbox</ion-label>\r\n    </ion-tab-button>\r\n\r\n    <ion-tab-button tab=\"profile\">\r\n      <ion-icon name=\"person-outline\"></ion-icon>\r\n      <ion-label>Profile</ion-label>\r\n    </ion-tab-button>\r\n  </ion-tab-bar>\r\n</ion-tabs>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_tabbar_tabbar_module_ts.js.map
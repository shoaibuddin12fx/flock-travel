
  cordova.define('cordova/plugin_list', function(require, exports, module) {
    module.exports = [
      {
          "id": "cordova-plugin-inappbrowser.inappbrowser",
          "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
          "pluginId": "cordova-plugin-inappbrowser",
        "clobbers": [
          "cordova.InAppBrowser.open"
        ]
        },
      {
          "id": "cordova-plugin-facebook-connect.FacebookConnectPlugin",
          "file": "plugins/cordova-plugin-facebook-connect/www/facebook-native.js",
          "pluginId": "cordova-plugin-facebook-connect",
        "clobbers": [
          "facebookConnectPlugin"
        ]
        }
    ];
    module.exports.metadata =
    // TOP OF METADATA
    {
      "cordova-plugin-facebook-connect": "3.2.0",
      "cordova-plugin-inappbrowser": "5.0.0"
    };
    // BOTTOM OF METADATA
    });
    
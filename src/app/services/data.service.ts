import { Injectable } from '@angular/core';
const destionations = require('../data/destinations.json');
const home_categories = require('../data/home_categories.json');
const trips = require('../data/trips.json');
const hotels = require('../data/hotels.json');
const flights = require('../data/flights.json');
const cards = require('../data/cards.json');

@Injectable({
  providedIn: 'root',
})
export class DataService {
  getDestinations() {
    return destionations;
  }

  getHomeCategories() {
    return home_categories;
  }

  getTrips() {
    return trips;
  }

  getHotels() {
    return hotels;
  }

  getFlights() {
    return flights;
  }

  getCards() {
    return cards;
  }
}

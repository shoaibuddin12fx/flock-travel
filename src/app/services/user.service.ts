import { Injectable } from '@angular/core';
const users = require('src/app/data/users.json');
@Injectable({
  providedIn: 'root',
})
export class UserService {
  user = null;
  constructor() {}

  login(formdata) {
    return new Promise((resolve) => {
      let record = users.find((x) => x.email == formdata['email']);
      if (record) {
        this.setToken(record['id']);
        this.user = record;
        resolve(record);
      } else {
        this.user = null;
        resolve(false);
      }
    });
  }

  getUser() {
    return {
      name: 'Jont Henry',
      location: 'San Francisco, CA',
      email: 'jonthenery@gmail.com',
      phone: '(239) 555-0108',
      dob: 'March 27, 1989',
      address: '6391 Elgin, St celina, Delaware',
    };
    // return new Promise(async (resolve) => {
    //   let token = await this.getToken();
    //   console.log(token);
    //   let record = users.find((x) => parseInt(x.id) == parseInt(token));
    //   if (record) {
    //     this.user = record;
    //     resolve(record);
    //   } else {
    //     this.user = null;
    //     resolve(false);
    //   }
    // });
  }

  setToken(token) {
    return localStorage.setItem('token', token);
  }

  removeToken() {
    return localStorage.removeItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }
}

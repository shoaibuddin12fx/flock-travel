import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DestinationsComponent } from './destinations.component';
import { IonicModule } from '@ionic/angular';
import { DestinationItemModule } from './destination-item/destination-item.module';

@NgModule({
  declarations: [DestinationsComponent],
  imports: [CommonModule, IonicModule, DestinationItemModule],
  exports: [DestinationsComponent],
})
export class DestinationsModule {}

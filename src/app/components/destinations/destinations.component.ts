import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: ['./destinations.component.scss'],
})
export class DestinationsComponent extends BasePage implements OnInit {
  destinations = [];

  constructor(injector: Injector) {
    super(injector);
  }
  ngOnInit() {
    this.destinations = this.dataService.getDestinations();
  }
  selectItem(item) {
    item.selected = !item.selected;
  }

  goToHome() {
    console.log('goToHome');
    this.modals.dismiss({ data: 'A' });
    // this.nav.push('pages/tabbar');
  }
}

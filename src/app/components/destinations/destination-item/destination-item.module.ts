import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { DestinationItemComponent } from './destination-item.component';

@NgModule({
  declarations: [DestinationItemComponent],
  imports: [CommonModule, IonicModule],
  exports: [DestinationItemComponent],
})
export class DestinationItemModule {}

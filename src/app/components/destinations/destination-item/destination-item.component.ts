import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'destination-item',
  templateUrl: './destination-item.component.html',
  styleUrls: ['./destination-item.component.scss'],
})
export class DestinationItemComponent implements OnInit {
  @Input() item;
  constructor() {}

  ngOnInit() {}
}

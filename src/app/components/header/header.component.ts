import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { SearchDestinationPage } from 'src/app/pages/search-destination/search-destination.page';
import { NavService } from 'src/app/services/basic/nav.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent extends BasePage implements OnInit {
  @Input() title = '';
  @Input() searchVisible = true;
  @Input() editVisible = false;
  @Input() backVisible = false;
  @Input() menuVisible = true;

  constructor(injector: Injector, public nav: NavService) {
    super(injector);
  }

  ngOnInit() {}

  goBack() {
    this.nav.pop();
  }

  gotoProfile() {
    this.nav.push('pages/profile');
  }

  gotoNotifications() {
    this.nav.push('pages/notifications');
  }

  search() {
    this.modals.present(SearchDestinationPage);
    //this.nav.push('pages/search-destination');
  }
}

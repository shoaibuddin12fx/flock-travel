import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrentLocationComponent } from './current-location.component';


@NgModule({
  declarations: [CurrentLocationComponent],
  imports: [
    CommonModule
  ],
  exports: [
    CurrentLocationComponent
  ]
})
export class CurrentLocationModule { }

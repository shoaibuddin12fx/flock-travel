import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ModalService } from 'src/app/services/basic/modal.service';
import { GoogleMaps, GoogleMap, CameraPosition, LatLng, GoogleMapsEvent } from '@ionic-native/google-maps';


@Component({
  selector: 'app-current-location',
  templateUrl: './current-location.component.html',
  styleUrls: ['./current-location.component.scss'],
})
export class CurrentLocationComponent implements OnInit {

  @ViewChild('map') mapElement: ElementRef;
  map: GoogleMap;
  GoogleMaps: any;

  
  constructor(public modals: ModalService,private _googleMaps: GoogleMaps) { }


  ngOnInit() {}

  ngAfterViewInit(){
    this.initMap();
  }

  initMap(){
    let element = this.mapElement.nativeElement;
    let loc: LatLng = new LatLng(40.7128, -74.0059);

    this.map = this._googleMaps.create(element,{ styles: []});

    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
        this.moveCamera(loc);
    });
  }

  moveCamera(loc: LatLng){
      let options= {
        target: loc,
        zoom: 15,
        tilt: 10
      }
      this.map.moveCamera(options);
  }

}



class cons {
  static secure = "s";
  static domain = "veenmeapi.thesupportonline.net";
  static default_part = "index.php/v1";

}

export const Config = {
  SERVICEURL: `http${cons.secure}://${cons.domain}/${cons.default_part}`
};

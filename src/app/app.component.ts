import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { NavService } from './services/basic/nav.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  constructor(
    private nav: NavService
  ) { }

  goTo(page){
    this.nav.navigateTo(page);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HotelLocationPage } from './hotel-location.page';

const routes: Routes = [
  {
    path: '',
    component: HotelLocationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HotelLocationPageRoutingModule {}

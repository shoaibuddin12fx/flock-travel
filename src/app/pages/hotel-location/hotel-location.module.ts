import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HotelLocationPageRoutingModule } from './hotel-location-routing.module';

import { HotelLocationPage } from './hotel-location.page';
import { HeaderModule } from 'src/app/components/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HotelLocationPageRoutingModule,
    HeaderModule,
  ],
  declarations: [HotelLocationPage],
})
export class HotelLocationPageModule {}

import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-hotel-location',
  templateUrl: './hotel-location.page.html',
  styleUrls: ['./hotel-location.page.scss'],
})
export class HotelLocationPage extends BasePage implements OnInit {
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  goToHotelDetail() {
    this.nav.push('pages/hotel-detail');
  }
}

import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-destination',
  templateUrl: './destination.page.html',
  styleUrls: ['./destination.page.scss'],
})
export class DestinationPage extends BasePage implements OnInit {
  destinations = [];

  constructor(injector: Injector) {
    super(injector);
  }
  ngOnInit() {
    this.destinations = this.dataService.getDestinations();
  }

  selectItem(item) {
    item.selected = !item.selected;
  }

  goToHome() {
    console.log('goToHome');

    this.modals.dismiss({ data: 'A' });
    this.nav.push('pages/tabbar');
  }
}

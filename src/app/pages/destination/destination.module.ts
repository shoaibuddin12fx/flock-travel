import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DestinationPageRoutingModule } from './destination-routing.module';

import { DestinationPage } from './destination.page';
import { DestinationItemModule } from 'src/app/components/destinations/destination-item/destination-item.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DestinationPageRoutingModule,
    DestinationItemModule,
  ],
  declarations: [DestinationPage],
})
export class DestinationPageModule {}

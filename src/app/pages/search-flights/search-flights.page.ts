import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-search-flights',
  templateUrl: './search-flights.page.html',
  styleUrls: ['./search-flights.page.scss'],
})
export class SearchFlightsPage extends BasePage implements OnInit {
  ways = [
    {
      name: 'One Way',
      is_selected: true,
    },
    {
      name: 'Round Trip',
      is_selected: false,
    },
    {
      name: 'Multi-City',
      is_selected: false,
    },
  ];

  adults = 2;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  typeSelected(item) {
    this.ways.filter((x) => x.is_selected)[0].is_selected = false;
    item.is_selected = true;
    //console.log(item);
  }

  searchFlights() {
    this.nav.push('pages/flights');
  }
}

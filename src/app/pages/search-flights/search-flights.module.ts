import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchFlightsPageRoutingModule } from './search-flights-routing.module';

import { SearchFlightsPage } from './search-flights.page';
import { HeaderModule } from 'src/app/components/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchFlightsPageRoutingModule,
    HeaderModule,
  ],
  declarations: [SearchFlightsPage],
})
export class SearchFlightsPageModule {}

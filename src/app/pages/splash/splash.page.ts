import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage extends BasePage implements OnInit {
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.initialize();
  }

  initialize() {
    setTimeout(() => {
      this.nav.push('pages/login');
    }, 2000);
  }

  goTo() {
    this.nav.push('pages/login');
  }
}

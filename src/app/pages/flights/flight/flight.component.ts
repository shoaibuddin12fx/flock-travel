import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.scss'],
})
export class FlightComponent implements OnInit {
  @Input() item;
  constructor() {}

  ngOnInit() {}
}

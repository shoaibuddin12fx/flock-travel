import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FlightComponent } from './flight.component';

@NgModule({
  declarations: [FlightComponent],
  imports: [CommonModule, IonicModule],
  exports: [FlightComponent],
})
export class FlightModule {}

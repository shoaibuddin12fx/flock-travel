import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.page.html',
  styleUrls: ['./flights.page.scss'],
})
export class FlightsPage extends BasePage implements OnInit {
  flights = [];
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.getFlights();
  }

  getFlights() {
    this.flights = this.dataService.getFlights();
  }

  getFlightDetails() {
    this.nav.push('pages/flights-detail');
  }
}

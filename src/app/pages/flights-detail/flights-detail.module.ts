import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FlightsDetailPageRoutingModule } from './flights-detail-routing.module';

import { FlightsDetailPage } from './flights-detail.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { FlightDetailsModule } from './flight-detail/flight-detail.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FlightsDetailPageRoutingModule,
    HeaderModule,
    FlightDetailsModule,
  ],
  declarations: [FlightsDetailPage],
})
export class FlightsDetailPageModule {}

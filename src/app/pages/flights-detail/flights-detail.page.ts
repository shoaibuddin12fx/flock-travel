import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-flights-detail',
  templateUrl: './flights-detail.page.html',
  styleUrls: ['./flights-detail.page.scss'],
})
export class FlightsDetailPage extends BasePage implements OnInit {
  list = [];
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.getFlightsDetail();
  }

  getFlightsDetail() {
    this.list = this.dataService.getFlights();
  }

  bookNow() {
    this.nav.push('pages/payment-methods');
  }
}

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'flight-detail',
  templateUrl: './flight-detail.component.html',
  styleUrls: ['./flight-detail.component.scss'],
})
export class FlightDetailComponent implements OnInit {
  @Input() item;
  constructor() {}

  ngOnInit() {}
}

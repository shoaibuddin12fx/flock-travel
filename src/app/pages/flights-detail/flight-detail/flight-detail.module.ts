import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FlightDetailComponent } from './flight-detail.component';

@NgModule({
  declarations: [FlightDetailComponent],
  imports: [CommonModule, IonicModule],
  exports: [FlightDetailComponent],
})
export class FlightDetailsModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FlightsDetailPage } from './flights-detail.page';

const routes: Routes = [
  {
    path: '',
    component: FlightsDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FlightsDetailPageRoutingModule {}

import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-payment-methods',
  templateUrl: './payment-methods.page.html',
  styleUrls: ['./payment-methods.page.scss'],
})
export class PaymentMethodsPage extends BasePage implements OnInit {
  cards = [];
  public slidesConfig = {
    slidesPerView: 1.1,
    spaceBetween: 20,
    initialSlide: 1,
    zoom: false,
    autoplayDisableOnInteraction: false,
    loop: true,
    speed: 500,
    breakpointsInverse: false,

    // autoplay: {
    //   delay: 2000,
    // },

    breakpoints: {
      992: {
        slidesPerView: 1,
        spaceBetween: 20,
        loop: false,
      },
    },
  };
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.cards = this.dataService.getCards();
  }

  goToHome() {
    this.nav.push('pages/tabbar');
  }
}

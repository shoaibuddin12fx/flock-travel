import {
  Component,
  ElementRef,
  Injector,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  GoogleMap,
  GoogleMaps,
  LatLng,
  GoogleMapsEvent,
} from '@ionic-native/google-maps';
import { ModalService } from 'src/app/services/basic/modal.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-current-location',
  templateUrl: './current-location.page.html',
  styleUrls: ['./current-location.page.scss'],
})
export class CurrentLocationPage extends BasePage implements OnInit {
  @ViewChild('map') mapElement: ElementRef;
  map: GoogleMap;
  GoogleMaps: any;

  constructor(
    injector: Injector,
    public modals: ModalService,
    private _googleMaps: GoogleMaps
  ) {
    super(injector);
  }

  ngOnInit() {}

  ngAfterViewInit() {
    // this.initMap();
  }

  initMap() {
    let element = this.mapElement.nativeElement;
    let loc: LatLng = new LatLng(40.7128, -74.0059);

    this.map = this._googleMaps.create(element, { styles: [] });

    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      this.moveCamera(loc);
    });
  }

  moveCamera(loc: LatLng) {
    let options = {
      target: loc,
      zoom: 15,
      tilt: 10,
    };
    this.map.moveCamera(options);
  }

  saveLocation() {
    this.nav.push('pages/destination');
  }
}

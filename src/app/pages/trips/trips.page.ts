import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-trips',
  templateUrl: './trips.page.html',
  styleUrls: ['./trips.page.scss'],
})
export class TripsPage extends BasePage implements OnInit {
  trips = [
    1,
    2,
    3
  ];
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    // this.trips = this.dataService.getTrips();
  }
}

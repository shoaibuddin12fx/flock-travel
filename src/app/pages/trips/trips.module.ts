import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TripsPageRoutingModule } from './trips-routing.module';

import { TripsPage } from './trips.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { TripComponentModule } from './trip/trip.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TripsPageRoutingModule,
    HeaderModule,
    TripComponentModule,
  ],
  declarations: [TripsPage],
})
export class TripsPageModule {}

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.scss'],
})
export class TripComponent implements OnInit {
  @Input() item;
  constructor() {}

  ngOnInit() {}
}

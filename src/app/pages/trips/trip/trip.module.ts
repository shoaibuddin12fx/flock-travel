import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TripComponent } from './trip.component';

@NgModule({
  declarations: [TripComponent],
  imports: [CommonModule, IonicModule],
  exports: [TripComponent],
})
export class TripComponentModule {}

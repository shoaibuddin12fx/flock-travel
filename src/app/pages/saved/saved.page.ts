import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-saved',
  templateUrl: './saved.page.html',
  styleUrls: ['./saved.page.scss'],
})
export class SavedPage extends BasePage implements OnInit {
  list;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.list = this.dataService.getDestinations();
  }
}

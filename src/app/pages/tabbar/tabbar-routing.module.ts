import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabbarPage } from './tabbar.page';

const routes: Routes = [
  {
    path: '',
    component: TabbarPage,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'explore',
      },
      {
        path: 'explore',
        loadChildren: () =>
          import('../explore/explore.module').then((m) => m.ExplorePageModule),
      },
      {
        path: 'saved',
        loadChildren: () => import('../saved/saved.module').then(m => m.SavedPageModule)
      },
      {
        path: 'inbox',
        loadChildren: () => import('../inbox/inbox.module').then(m => m.InboxPageModule)
      },
      {
        path: 'trips',
        loadChildren: () =>
          import('../trips/trips.module').then((m) => m.TripsPageModule),
      },
      {
        path: 'profile',
        loadChildren: () =>
          import('./../edit-profile/edit-profile.module').then(
            (m) => m.EditProfilePageModule
          ),
      },
    ]
    // {
    //   path: 'races',
    //   loadChildren: () =>
    //     import('./../races/races.module').then((m) => m.RacesPageModule),
    // },
    // {
    //   path: 'news',
    //   loadChildren: () =>
    //     import('./../news/news.module').then((m) => m.NewsPageModule),
    // },
    // {
    //   path: 'timetable',
    //   loadChildren: () =>
    //     import('./../timetable/timetable.module').then(
    //       (m) => m.TimetablePageModule
    //     ),
    // },
    // {
    //   path: 'videos',
    //   loadChildren: () =>
    //     import('./../videos/videos.module').then((m) => m.VideosPageModule),
    // },
    // {
    //   path: 'store',
    //   loadChildren: () =>
    //     import('./../store/store.module').then((m) => m.StorePageModule),
    // },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabbarPageRoutingModule { }

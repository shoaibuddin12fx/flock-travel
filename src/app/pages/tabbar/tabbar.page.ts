import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonTabs } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
// import { HomePage } from '../home/home.page';

@Component({
  selector: 'app-tabbar',
  templateUrl: './tabbar.page.html',
  styleUrls: ['./tabbar.page.scss'],
})
export class TabbarPage extends BasePage implements OnInit {

  @ViewChild('tabs', { static: false }) tabs: IonTabs;
  static selectedTab;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }

  setCurrentTab() {
    TabbarPage.selectedTab = this.tabs.getSelected();
  }

}

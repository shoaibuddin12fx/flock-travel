import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'splash',
    pathMatch: 'full',
  },
  {
    path: 'splash',
    loadChildren: () =>
      import('./splash/splash.module').then((m) => m.SplashPageModule),
  },

  {
    path: 'signup',
    loadChildren: () =>
      import('./signup/signup.module').then((m) => m.SignupPageModule),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginPageModule),
  },
  // {
  //   path: 'home',
  //   loadChildren: () =>
  //     import('./home/home.module').then((m) => m.HomePageModule),
  // },
  {
    path: 'tabbar',
    loadChildren: () =>
      import('./tabbar/tabbar.module').then((m) => m.TabbarPageModule),
  },
  {
    path: 'notifications',
    loadChildren: () =>
      import('./notifications/notifications.module').then(
        (m) => m.NotificationsPageModule
      ),
  },
  {
    path: 'edit-profile',
    loadChildren: () =>
      import('./edit-profile/edit-profile.module').then(
        (m) => m.EditProfilePageModule
      ),
  },
  {
    path: 'destination',
    loadChildren: () =>
      import('./destination/destination.module').then(
        (m) => m.DestinationPageModule
      ),
  },
  {
    path: 'book-initial',
    loadChildren: () =>
      import('./book-initial/book-initial.module').then(
        (m) => m.BookInitialPageModule
      ),
  },
  {
    path: 'trips',
    loadChildren: () =>
      import('./trips/trips.module').then((m) => m.TripsPageModule),
  },  {
    path: 'hotels',
    loadChildren: () => import('./hotels/hotels.module').then( m => m.HotelsPageModule)
  },
  {
    path: 'hotel-location',
    loadChildren: () => import('./hotel-location/hotel-location.module').then( m => m.HotelLocationPageModule)
  },
  {
    path: 'hotel-detail',
    loadChildren: () => import('./hotel-detail/hotel-detail.module').then( m => m.HotelDetailPageModule)
  },
  {
    path: 'search-flights',
    loadChildren: () => import('./search-flights/search-flights.module').then( m => m.SearchFlightsPageModule)
  },
  {
    path: 'flights',
    loadChildren: () => import('./flights/flights.module').then( m => m.FlightsPageModule)
  },
  {
    path: 'flights-detail',
    loadChildren: () => import('./flights-detail/flights-detail.module').then( m => m.FlightsDetailPageModule)
  },
  {
    path: 'payment-methods',
    loadChildren: () => import('./payment-methods/payment-methods.module').then( m => m.PaymentMethodsPageModule)
  },
  {
    path: 'search-destination',
    loadChildren: () => import('./search-destination/search-destination.module').then( m => m.SearchDestinationPageModule)
  },
  {
    path: 'current-location',
    loadChildren: () => import('./current-location/current-location.module').then( m => m.CurrentLocationPageModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}

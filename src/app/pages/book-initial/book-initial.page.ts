import { Component, OnInit } from '@angular/core';
import { NavService } from 'src/app/services/basic/nav.service';

@Component({
  selector: 'app-book-initial',
  templateUrl: './book-initial.page.html',
  styleUrls: ['./book-initial.page.scss'],
})
export class BookInitialPage implements OnInit {
  constructor(public nav: NavService) {}

  ngOnInit() {}

  step = 1;
  person_count = 1;

  back() {
    if (this.step === 1) this.nav.pop();
    else this.step--;
  }

  next() {
    this.step++;
  }

  addRemovePerson(isPlus) {
    if (isPlus) this.person_count++;
    else if (this.person_count > 1) this.person_count--;
  }

  goToHotels() {
    this.nav.push('pages/hotels');
  }
}

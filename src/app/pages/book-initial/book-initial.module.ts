import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookInitialPageRoutingModule } from './book-initial-routing.module';

import { BookInitialPage } from './book-initial.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookInitialPageRoutingModule
  ],
  declarations: [BookInitialPage]
})
export class BookInitialPageModule {}

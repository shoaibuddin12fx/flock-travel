import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookInitialPage } from './book-initial.page';

const routes: Routes = [
  {
    path: '',
    component: BookInitialPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookInitialPageRoutingModule {}

import { Component, Injector, OnInit } from '@angular/core';
import { CurrentLocationComponent } from 'src/app/components/current-location/current-location.component';
import { DestinationsComponent } from 'src/app/components/destinations/destinations.component';
import { BasePage } from '../base-page/base-page';
import { DestinationPage } from '../destination/destination.page';

@Component({
  selector: 'app-explore',
  templateUrl: './explore.page.html',
  styleUrls: ['./explore.page.scss'],
})
export class ExplorePage extends BasePage implements OnInit {
  public slidesConfig = {
    slidesPerView: 1.6,
    spaceBetween: 20,
    initialSlide: 1,
    zoom: false,
    autoplayDisableOnInteraction: false,
    loop: true,
    speed: 500,
    breakpointsInverse: false,

    // autoplay: {
    //   delay: 2000,
    // },

    breakpoints: {
      992: {
        slidesPerView: 1,
        spaceBetween: 20,
        loop: false,
      },
    },
  };

  slides = [];
  news = [];
  races = [];
  categories = [];
  destinations = [];
  adventures = [
    {
      image: 'assets/races/race2.png',
    },
    {
      image: 'assets/races/race3.jpeg',
    },
    {
      image: 'assets/races/race2.png',
    },
  ];

  list = ['1', '2', '3'];

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.initialize();
  }

  initialize() {
    this.categories = this.dataService.getHomeCategories();
    this.destinations = this.dataService.getDestinations();
  }

  raceEventDetail(id) {
    this.nav.push('pages/tabbar/races');
  }

  openCurrentLocModal() {
    this.modals.present(CurrentLocationComponent);
  }

  bookInit() {
    this.nav.push('pages/book-initial');
  }

  segmentChanged($event) {
    console.log($event);
  }

  openDestinations() {
    this.modals.present(DestinationPage);
  }

  categorySelected(item) {
    this.categories.filter((x) => x.selected)[0].selected = false;
    item.selected = true;
    //console.log(item);
  }

  goToSteps() {
    this.nav.push('pages/book-initial');
  }
}

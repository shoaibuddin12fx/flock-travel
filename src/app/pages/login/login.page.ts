import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Browser } from '@capacitor/browser';
import { BasePage } from '../base-page/base-page';
import { SignupPage } from '../signup/signup.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BasePage implements OnInit {
  aForm: FormGroup;
  loading = false;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.setupForm();
  }

  setupForm() {
    const re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      email: [
        'john_smith22@gmail.com',
        Validators.compose([Validators.required, Validators.email]),
      ],
      password: [
        '123456',
        Validators.compose([
          Validators.minLength(6),
          Validators.maxLength(30),
          Validators.required,
        ]),
      ],
    });
  }

  openSignup() {
    this.nav.navigateTo('pages/signup');
  }

  async login() {
    // this.nav.push('pages/tabbar');
    if (!this.aForm.valid) {
      this.utility.presentFailureToast('Pleae fill all fields properly');
      return;
    }

    const formdata = this.aForm.value;
    this.loading = true;
    const user = await this.users.login(formdata);
    if (user) {
      this.nav.push('pages/tabbar');
    } else {
    }
    this.loading = false;
  }
}

import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-hotel-detail',
  templateUrl: './hotel-detail.page.html',
  styleUrls: ['./hotel-detail.page.scss'],
})
export class HotelDetailPage extends BasePage implements OnInit {
  categories = [
    {
      name: 'Overview',
      isSelected: true,
    },
    {
      name: 'Amenities',
      isSelected: false,
    },
    {
      name: 'Reviews',
      isSelected: false,
    },
  ];
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  categorySelected(item) {
    this.categories.filter((x) => x.isSelected)[0].isSelected = false;
    item.isSelected = true;
    //console.log(item);
  }

  goToSearchFlights() {
    this.nav.push('pages/search-flights');
  }

  goBack() {
    this.nav.pop();
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchDestinationPageRoutingModule } from './search-destination-routing.module';

import { SearchDestinationPage } from './search-destination.page';
import { DestinationModule } from './destination/destination.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchDestinationPageRoutingModule,
    DestinationModule,
  ],
  declarations: [SearchDestinationPage],
})
export class SearchDestinationPageModule {}

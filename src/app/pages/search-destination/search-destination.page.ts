import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-search-destination',
  templateUrl: './search-destination.page.html',
  styleUrls: ['./search-destination.page.scss'],
})
export class SearchDestinationPage extends BasePage implements OnInit {
  list;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.getDestinations();
  }

  getDestinations() {
    this.list = this.dataService.getDestinations();
  }

  close() {
    this.modals.dismiss({ data: 'A' });
  }
}

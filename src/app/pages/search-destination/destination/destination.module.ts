import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { DestinationComponent } from './destination.component';

@NgModule({
  declarations: [DestinationComponent],
  imports: [CommonModule, IonicModule],
  exports: [DestinationComponent],
})
export class DestinationModule {}

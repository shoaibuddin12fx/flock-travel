import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'destination',
  templateUrl: './destination.component.html',
  styleUrls: ['./destination.component.scss'],
})
export class DestinationComponent implements OnInit {
  @Input() item;
  constructor() {}

  ngOnInit() {}
}

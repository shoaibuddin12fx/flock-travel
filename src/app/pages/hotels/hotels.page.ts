import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.page.html',
  styleUrls: ['./hotels.page.scss'],
})
export class HotelsPage extends BasePage implements OnInit {
  hotels = [];
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.hotels = this.dataService.getHotels();
  }

  goToHotelLocation() {
    this.nav.push('pages/hotel-location');
  }
}

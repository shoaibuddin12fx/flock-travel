import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HotelsPageRoutingModule } from './hotels-routing.module';

import { HotelsPage } from './hotels.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { HotelRowModule } from './hotel-row/hotel-row.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HotelsPageRoutingModule,
    HeaderModule,
    HotelRowModule,
  ],
  declarations: [HotelsPage],
})
export class HotelsPageModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { HotelRowComponent } from './hotel-row.component';

@NgModule({
  declarations: [HotelRowComponent],
  imports: [CommonModule, IonicModule],
  exports: [HotelRowComponent],
})
export class HotelRowModule {}

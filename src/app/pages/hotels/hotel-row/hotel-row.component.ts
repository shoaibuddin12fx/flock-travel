import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'hotel-row',
  templateUrl: './hotel-row.component.html',
  styleUrls: ['./hotel-row.component.scss'],
})
export class HotelRowComponent implements OnInit {
  @Input() item;
  constructor() {}

  ngOnInit() {}
}
